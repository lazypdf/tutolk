<?php

/**
 * @version     1.0.0
 * @package     com_tutolk
 * @copyright   Copyright (C) 2015. Todos los derechos reservados.
 * @license     Licencia Pública General GNU versión 2 o posterior. Consulte LICENSE.txt
 * @author      aficat <kim@aficat.com> - http://www.afi.cat
 */
// No direct access
defined('_JEXEC') or die;

require_once JPATH_COMPONENT . '/controller.php';

/**
 * User controller class.
 */
class TutolkControllerMeeting extends TutolkController {

	/**
	* Proxy for getModel.
	* @since	1.6
	*/
	public function getModel($name = 'Meeting', $prefix = 'TutolkModel') 
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));
		return $model;
	}

	/**
	   * Method to save a message data.
	   *
	   * @return	void
	   * @since	1.6
	*/
	public function save() {
		
			JRequest::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
			// Initialise variables.
			$model	= $this->getModel();
	 
			// Get the data from the form POST
			$data = JRequest::getVar('jform', array(), 'post', 'array');
		
			if($model->store($data)) 
			{
				$msg = JText::_('COM_TUTOLK_MEETING_SUCCESS_CREATE');
				$this->setRedirect(JRoute::_('index.php?option=com_tutolk&view=meetings&Itemid=132'), $msg, 'success');
			}
			else {
				$this->setRedirect(JRoute::_('index.php?option=com_tutolk&view=meetings&Itemid=132'), JText::_('COM_TUTOLK_MEETING_ERROR_CREATE'), 'error');
			}
	}
    
    	public function end() {
    	
    		JSession::checkToken( 'get' ) or die( 'Invalid Token' );

		$db = JFactory::getDbo();
		$id = JRequest::getVar('id');
		
    		$url = TutolkHelper::getEndMeetingUrl($id); 
    		
    		$db->setQuery('update #__tutolk_meetings set status = 3 where id = '.$id);
    		if($db->query()) {

			$curl = curl_init($url);
			$resp = curl_exec($curl);
			curl_close($curl);

			$msg = JText::_('COM_TUTOLK_MEETING_SUCCESS_END');
			$this->setRedirect(JRoute::_('index.php?option=com_tutolk&view=meetings&Itemid=132'), $msg, 'success');
		} else {
			$this->setRedirect(JRoute::_('index.php?option=com_tutolk&view=meetings&Itemid=132'), JText::_('COM_TUTOLK_MEETING_ERROR_END'), 'error');
		}
    	}

	public function remove() {

		JSession::checkToken( 'get' ) or die( 'Invalid Token' );

		$db = JFactory::getDbo();
		$id = JRequest::getVar('id');

		$db->setQuery('select attendees from #__tutolk_meetings where id = '.$id);
		$attendees = $db->loadResult();
		
		$db->setQuery('delete from #__tutolk_meetings where id = '.$id);
		if($db->query()) {

			//send message to attendees...
			$subject = JText::_('COM_TUTOLK_MEETING_REMOVED_SUBJECT');

			foreach(implode(',', $attendees) as $att) {

				$body	 = JText::sprintf('COM_TUTOLK_MEETING_REMOVED_BODY', $id);

				TutolkHelper::sendMessage($att, 0, $subject, $body, $time);
				TutolkHelper::sendEmail($subject, $body, JFactory::getUser($att)->email);
			}

			$msg = JText::_('COM_TUTOLK_MEETING_SUCCESS_REMOVE');
			$this->setRedirect(JRoute::_('index.php?option=com_tutolk&view=meetings&Itemid=132'), $msg, 'success');
		} else {
			$this->setRedirect(JRoute::_('index.php?option=com_tutolk&view=meetings&Itemid=132'), JText::_('COM_TUTOLK_MEETING_ERROR_REMOVE'), 'error');
		}
	}

}
