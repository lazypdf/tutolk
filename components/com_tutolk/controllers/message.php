<?php

/**
 * @version     1.0.0
 * @package     com_tutolk
 * @copyright   Copyright (C) 2015. Todos los derechos reservados.
 * @license     Licencia Pública General GNU versión 2 o posterior. Consulte LICENSE.txt
 * @author      aficat <kim@aficat.com> - http://www.afi.cat
 */
// No direct access
defined('_JEXEC') or die;

require_once JPATH_COMPONENT . '/controller.php';

/**
 * User controller class.
 */
class TutolkControllerMessage extends TutolkController {

	/**
	* Proxy for getModel.
	* @since	1.6
	*/
	public function getModel($name = 'Message', $prefix = 'TutolkModel') 
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));
		return $model;
	}

	/**
     * Method to save a message data.
     *
     * @return	void
     * @since	1.6
     */
    public function save() {
        
        	JRequest::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
		// Initialise variables.
		$model	= $this->getModel();
 
		// Get the data from the form POST
		$data = JRequest::getVar('jform', array(), 'post', 'array');
		
		//check if is friend...
		$db = JFactory::getDbo();
		$user = JFactory::getUser();
		$db->setQuery(	'select userid from #__tutolk_friends '.
				'where userid = '.$user->id.' and friendid = '.$data['userid'].' and status = 1');
		if($db->loadResult() > 0) {
		
			if($model->store($data)) 
			{
				$msg = JText::_('COM_TUTOLK_MSG_SUCCESS_SENT');
				$this->setRedirect(JRoute::_('index.php?option=com_tutolk&view=messages&Itemid=130'), $msg, 'success');
			}
			else {
				$this->setRedirect(JRoute::_('index.php?option=com_tutolk&view=messages&Itemid=130'), JText::_('COM_TUTOLK_MSG_ERROR_SENT'), 'error');
			}
		} else {
			$this->setRedirect(JRoute::_('index.php?option=com_tutolk&view=messages&Itemid=130'), JText::_('COM_TUTOLK_MSG_NO_FRIEND'), 'error');
		}
    }
    
    public function markAllRead() {
	
		JSession::checkToken( 'get' ) or die( 'Invalid Token' );

		$db     = JFactory::getDbo();
		$user   = JFactory::getUser();

		$db->setQuery('update #__tutolk_messages set status = 1 where userid = '.$user->id);
		$result = $db->query();
		$rows = $db->getAffectedRows();
		if($result) {
			if($rows > 0) {
				$msg = JText::_('Messages successfully mark as readed');
				$this->setRedirect(JRoute::_('index.php?option=com_tutolk&view=messages'), $msg, 'success');
			} else {
				$msg = JText::_('No unread messages in your inbox.');
				$this->setRedirect(JRoute::_('index.php?option=com_tutolk&view=messages'), $msg, 'warning');
			}
		} else {
			$msg = JText::_('An error occurs trying to mark as read your messages.');
			$this->setRedirect(JRoute::_('index.php?option=com_tutolk&view=messages'), $msg, 'error');
		}
	}
    
    /**
     	* Method to delete a message.
     	*/
	public function deleteMessage() {

		JSession::checkToken( 'get' ) or die( 'Invalid Token' );
		
		$db 		= JFactory::getDbo();
		$user 		= JFactory::getUser();
		$jinput 	= JFactory::getApplication()->input;
		$id 		= $jinput->get('id', '', 'int');
		$result         = false;

		//check if is message owner
		$db->setQuery('select userid from #__tutolk_messages where id = '.$id);
		if($user->id == $db->loadResult()) {
			$db->setQuery('delete from #__tutolk_messages where id = '.$id);
			$result = $db->query();
		}
		
		if($result) {
			$msg = JText::_('COM_TUTOLK_MEESSAGE_DELETED_SUCCESS');
			$this->setRedirect(JRoute::_('index.php?option=com_tutolk&view=messages'), $msg, 'success');
		} else {
			$msg = JText::_('COM_TUTOLK_MEESSAGE_DELETED_ERROR');
			$this->setRedirect(JRoute::_('index.php?option=com_tutolk&view=messages'), $msg, 'error');
		}
	}
	
	public function readMessage() {

		JSession::checkToken( 'get' ) or die( 'Invalid Token' );
		
		$db 		= JFactory::getDbo();
		$user 		= JFactory::getUser();
		$jinput 	= JFactory::getApplication()->input;
		$id 		= $jinput->get('id', '', 'int');
		$result         = false;

		//check if is message owner
		$db->setQuery('select userid from #__tutolk_messages where id = '.$id);
		if($user->id == $db->loadResult()) {
			$db->setQuery('update #__tutolk_messages set status = 1 where id = '.$id);
			$result = $db->query();
		} else {
			$msg = JText::_('COM_TUTOLK_MEESSAGE_READ_NO_OWNER');
			$this->setRedirect(JRoute::_('index.php?option=com_tutolk&view=messages'), $msg, 'error');	
		}
		
		if($result) {
			$msg = JText::_('COM_TUTOLK_MEESSAGE_READ_ERROR');
			$this->setRedirect(JRoute::_('index.php?option=com_tutolk&view=messages'), $msg, 'success');
		} else {
			$msg = JText::_('COM_TUTOLK_MEESSAGE_READ_ERROR');
			$this->setRedirect(JRoute::_('index.php?option=com_tutolk&view=messages'), $msg, 'error');
		}
	}
	
	public function getMessages() {
		
		$db 		= JFactory::getDbo();
		$user 		= JFactory::getUser();
		$response       = array();

		//check if is message owner
		$db->setQuery('select count(id) as count from #__tutolk_messages where notified = 0 and userid = '.$user->id);
		$response['count'] = $db->loadResult();
		
		//update notified to only show one time the message
		$db->setQuery('update #__tutolk_messages set notified = 1 where userid = '.$user->id);
		$db->query();
		
		echo json_encode($response);
	}
	
	public function unreadMessage() {

		JSession::checkToken( 'get' ) or die( 'Invalid Token' );
		
		$db 		= JFactory::getDbo();
		$user 		= JFactory::getUser();
		$jinput 	= JFactory::getApplication()->input;
		$id 		= $jinput->get('id', '', 'int');
		$result         = false;

		//check if is message owner
		$db->setQuery('select userid from #__tutolk_messages where id = '.$id);
		if($user->id == $db->loadResult()) {
			$db->setQuery('update #__tutolk_messages set status = 0 where id = '.$id);
			$result = $db->query();
		} else {
			$msg = JText::_('COM_TUTOLK_MEESSAGE_READ_NO_OWNER');
			$this->setRedirect(JRoute::_('index.php?option=com_tutolk&view=messages'), $msg, 'error');	
		}
		
		if($result) {
			$msg = JText::_('COM_TUTOLK_MEESSAGE_UNREAD_ERROR');
			$this->setRedirect(JRoute::_('index.php?option=com_tutolk&view=messages'), $msg, 'success');
		} else {
			$msg = JText::_('COM_TUTOLK_MEESSAGE_UNREAD_ERROR');
			$this->setRedirect(JRoute::_('index.php?option=com_tutolk&view=messages'), $msg, 'error');
		}
	}

}
