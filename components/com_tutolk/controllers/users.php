<?php
/**
 * @version     1.0.0
 * @package     com_tutolk
 * @copyright   Copyright (C) 2015. Todos los derechos reservados.
 * @license     Licencia Pública General GNU versión 2 o posterior. Consulte LICENSE.txt
 * @author      aficat <kim@aficat.com> - http://www.afi.cat
 */

// No direct access.
defined('_JEXEC') or die;

require_once JPATH_COMPONENT.'/controller.php';

/**
 * Users list controller class.
 */
class TutolkControllerUsers extends TutolkController
{
	/**
	 * Proxy for getModel.
	 * @since	1.6
	 */
	public function &getModel($name = 'Users', $prefix = 'TutolkModel', $config = array())
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));
		return $model;
	}

	// get online status
	public function isOnline() 
	{
		$userid = JRequest::getVar('userid', '', 'get');
		if($userid == 'undefined' || $userid == '') { return; }
		$response = array();
		$db	= JFactory::getDbo();
		$query	= $db->getQuery(true);
		$query->select('session_id');
		$query->from('#__session');
		$query->where('userid = '.$userid);
		$query->where('client_id = 0');
		$db->setQuery($query);
		if($db->loadResult() != null) {
			$response['status'] = 'online';
		} else {
			$response['status'] = 'offline';
		}
		echo json_encode($response);
	}
}
