<?php

/**
 * @version     1.0.0
 * @package     com_tutolk
 * @copyright   Copyright (C) 2015. Todos los derechos reservados.
 * @license     Licencia Pública General GNU versión 2 o posterior. Consulte LICENSE.txt
 * @author      aficat <kim@aficat.com> - http://www.afi.cat
 */
// No direct access
defined('_JEXEC') or die;

require_once JPATH_COMPONENT . '/controller.php';

/**
 * User controller class.
 */
class TutolkControllerProfile extends TutolkController {

	/**
	* Proxy for getModel.
	* @since	1.6
	*/
	public function getModel($name = 'Profile', $prefix = 'TutolkModel') 
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));
		return $model;
	}

    /**
     * Method to save a user's profile data.
     *
     * @return	void
     * @since	1.6
     */
    public function save() {
        
        	JRequest::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
		// Initialise variables.
		$model	= $this->getModel();
 
		// Get the data from the form POST
		$data = JRequest::getVar('jform', array(), 'post', 'array');
		
		if($model->store($data)) 
		{
			$this->upload();
			$this->uploadVideo();
			$msg = JText::_('COM_TUTOLK_SUCCESS_SAVED');
			$this->setRedirect(JRoute::_('index.php?option=com_tutolk&view=profile&Itemid=112'), $msg, 'success');
		}
		else {
			JError::raiseWarning('36', JText::_('COM_TUTOLK_ERROR_SAVED'));
		}
    }

	function upload()
	{
		$path = JPATH_ROOT . DS . "images" . DS . "users" . DS;
		$data = JRequest::getVar('image-data');
		if($data != "") {
			$data = str_replace('data:image/png;base64,', '', $data);
			$data = str_replace(' ', '+', $data);
			$data = base64_decode($data);

			$db = JFactory::getDbo();

			//$return = JRoute::_('index.php?option=com_tutolk&view=profile&Itemid=112');

			//Set up the source and destination of the file
			$filename = uniqid() . '.png';
			$file = $path . $filename;
			$success = file_put_contents($file, $data);
			$db->setQuery('select image from #__tutolk_users where userid = '.JFactory::getUser()->id);
			$old_image = $db->loadResult();
		      	//modify database...
		      	$db->setQuery('update #__tutolk_users set image = '.$db->quote($filename).' where userid = '.JFactory::getUser()->id);
		      	$db->query();
			//delete old image...
			if($old_image != '') {
				unlink($path . $old_image);
			}
			$source_img = imagecreatefromstring($data);
			//$rotated_img = imagerotate($source_img, 90, 0); // rotate with angle 90 here
			//$imageSave = imagejpeg($source_img, $file, 10);
			imagedestroy($source_img);	
		}      	
	}

	function uploadBg()
	{
		$file = JRequest::getVar('bg', null, 'files', 'array');

		$db = JFactory::getDbo();
		$db->setQuery('select background from #__tutolk_users where userid = '.JFactory::getUser()->id);
		$last_image = $db->loadResult();
		$return = JRoute::_('index.php?option=com_tutolk&view=profile&Itemid=112');
		$allowed = array('jpg', 'jpeg', 'gif', 'png');

		jimport('joomla.filesystem.file');
		 
		//Clean up filename to get rid of strange characters like spaces etc
		$filename = JFile::makeSafe($file['name']);
		 
		//Set up the source and destination of the file
		$src = $file['tmp_name'];
		$dest = JPATH_ROOT . DS . "images" . DS . "users" . DS . "backgrounds" . DS . $filename;

		//check width and height...
		$image_info = getimagesize($src);
		$width = $image_info[0];
		$height = $image_info[1];
		if ($width >= 1500 && $height >= 350) {

			$extension = strtolower(JFile::getExt($filename)); 
			//First check if the file has the right extension
			if ( in_array($extension, $allowed) ) {
			   if ( JFile::upload($src, $dest) ) {
			      	//modify database...
			      	$db->setQuery('update #__tutolk_users set background = '.$db->quote($filename).' where userid = '.JFactory::getUser()->id);
			      	$db->query();
				//delete old image...
				if($last_image != '') {
					unlink(JPATH_ROOT . DS . "images" . DS . "users" . DS . "backgrounds" . DS . $last_image);
				}
				$msg = JText::_('COM_TUTOLK_TASK_UPLOAD_SUCCESS');
			      	$this->setRedirect($return, $msg, 'success');
			      
			   } else {
			      $this->setRedirect($return, JText::_('COM_TUTOLK_TASK_UPLOAD_ERROR'), 'error');
			   }
			} else {
			   $this->setRedirect($return, JText::_('COM_TUTOLK_TASK_UPLOAD_ONLY_IMAGES'), 'error');
			}

		} else {
			$this->setRedirect($return, JText::_('COM_TUTOLK_TASK_UPLOAD_ERROR_SIZE'), 'error'); 
		}
	}

	function uploadVideo()
	{
		$file = JRequest::getVar('video', null, 'files', 'array');
		if($file['name'] == '') { return; }
		$app = JFactory::getApplication();
		$db = JFactory::getDbo();
		$db->setQuery('select video from #__tutolk_users where userid = '.JFactory::getUser()->id);
		$last_video = $db->loadResult();
		$allowed = array('mp4');

		jimport('joomla.filesystem.file');
		 
		//Clean up filename to get rid of strange characters like spaces etc
		$filename = JFile::makeSafe($file['name']);
		 
		//Set up the source and destination of the file
		$src = $file['tmp_name'];
		$dest = JPATH_ROOT . DS . "images" . DS . "users" . DS . "videos" . DS . $filename;
		$extension = strtolower(JFile::getExt($filename)); 
		//First check if the file has the right extension and size
		if ( in_array($extension, $allowed) && $file['size'] < 8485760 ) {
		   if ( JFile::upload($src, $dest) ) {
		      	//modify database...
		      	$db->setQuery('update #__tutolk_users set video = '.$db->quote($filename).' where userid = '.JFactory::getUser()->id);
		      	$db->query();
			//delete old image...
			if($last_video != '') {
				unlink(JPATH_ROOT . DS . "images" . DS . "users" . DS . "videos" . DS . $last_video);
			}    
		   } else {
		      $app->enqueueMessage(JText::_('COM_TUTOLK_TASK_VUPLOAD_ERROR'), 'error');
		   }
		} else {
		   $app->enqueueMessage(JText::_('COM_TUTOLK_TASK_VUPLOAD_ONLY_MP4'.$extension), 'error');
		}
	}

	function deleteAccount() 
	{
		$pwd  = JRequest::getVar('pwd', 0, 'post');
		$db   = JFactory::getDbo();
		$user = JFactory::getUser();

		//check if password is correct...
		jimport('joomla.user.helper');
 		$pass = JUserHelper::hashPassword($pwd);
		if($user->password === $pass)  {

			$db->setQuery("update #__tutolk_users set status = 0, sex = '', short_description = '', description = '', country = '', image = '', background = '', tags = '', hour_from = '', hour_to = '', video = '' where userid = ".$user->id);
			$db->query();
			$db->setQuery('delete from #__tutolk_friends where userid = '.$user->id);
			$db->query();
			$db->setQuery('delete from #__tutolk_friends where friendid = '.$user->id);
			$db->query();
			$db->setQuery('delete from #__session where userid = '.$user->id);
			$db->query();

			$this->setRedirect('index.php', JText::_('COM_TUTOLK_ACCOUNT_DELETED'));
		} else {
			$this->setRedirect('index.php?option=com_tutolk&view=profile', JText::_('COM_TUTOLK_ACCOUNT_DELETE_ERROR'));
		}
	}

}
