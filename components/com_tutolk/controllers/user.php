<?php

/**
 * @version     1.0.0
 * @package     com_tutolk
 * @copyright   Copyright (C) 2015. Todos los derechos reservados.
 * @license     Licencia Pública General GNU versión 2 o posterior. Consulte LICENSE.txt
 * @author      aficat <kim@aficat.com> - http://www.afi.cat
 */
// No direct access
defined('_JEXEC') or die;

require_once JPATH_COMPONENT . '/controller.php';

/**
 * User controller class.
 */
class TutolkControllerUser extends TutolkController {

	/**
	  * Method to send email to a friendship request
	  *
	  * @since	1.6
	*/
	public function friendshipRequest() {
      
		JSession::checkToken( 'get' ) or die( 'Invalid Token' );
		$id = JFactory::getApplication()->input->get('userid');
		$user   = JFactory::getUser();
		$db     = JFactory::getDbo();
		$config = JFactory::getConfig();
		$result = false;

		//get the userid from tutolk table
		$db->setQuery('select userid from #__tutolk_users where id = '.$id);
		$userid = $db->loadResult();

		//send email to requested user...
		$vlink   = 'http://www.tutolk.com/index.php?option=com_tutolk&task=user.validate&user='.$user->id.'&friend='.$userid;
		$subject = JText::_('COM_TUTOLK_FRIENDSHIP_REQUEST_FRIEND_SUBJECT');
		$body    = JText::sprintf('COM_TUTOLK_FRIENDSHIP_REQUEST_FRIEND_BODY', $user->username, $vlink);
		TutolkHelper::sendEmail($subject, $body, JFactory::getUser($userid)->email);
		TutolkHelper::sendMessage($userid, 0, $subject, $body, time());

		//create database entry for a pending status of this friendship...
		$friend = new stdClass();
		$friend->userid   = $user->id;
		$friend->friendid = $userid;
		$friend->status   = 0;
		if($db->insertObject('#__tutolk_friends', $friend)) {

			$friend2 = new stdClass();
			$friend2->userid   = $userid;
			$friend2->friendid = $user->id;
			$friend2->status   = 0;
			$result = $db->insertObject('#__tutolk_friends', $friend2);
		}

		if($result) {
			$msg = JText::_('COM_TUTOLK_FRIENDSHIP_REQUEST_SUCCESS');
			$type = 'message';
		} else {
			$msg = JText::_('COM_TUTOLK_FRIENDSHIP_REQUEST_ERROR');
			$type = 'error';
		}
		$this->setRedirect(JRoute::_('index.php?option=com_tutolk&view=user&id='.$id), $msg, $type);

	}

	/**
	  * Method to decline a friendship
	  *
	  * @since	1.6
	*/
	public function friendshipDecline() {
      
		JSession::checkToken( 'get' ) or die( 'Invalid Token' );
		$id = JFactory::getApplication()->input->get('userid');
		$user   = JFactory::getUser();
		$db     = JFactory::getDbo();
		$config = JFactory::getConfig();
		$result0 = false;
		$result1 = false;

		//get the userid from tutolk table
		$db->setQuery('select userid from #__tutolk_users where id = '.$id);
		$userid = $db->loadResult();

		$db->setQuery('delete from #__tutolk_friends where userid = '.$user->id. ' and friendid = '.$id);
		$result0 = $db->query();
		$db->setQuery('delete from #__tutolk_friends where friendid = '.$user->id. ' and userid = '.$id);
		$result1 = $db->query();

		if($result0 && $result1) {
			$msg = JText::_('COM_TUTOLK_FRIENDSHIP_DECLINE_SUCCESS');
			$type = 'message';
		} else {
			$msg = JText::_('COM_TUTOLK_FRIENDSHIP_DECLINE_ERROR');
			$type = 'error';
		}
		$this->setRedirect(JRoute::_('index.php?option=com_tutolk&view=user&id='.$id), $msg, $type);

	}

	public function validate() {

		$userid    = JRequest::getVar('user', 0, 'get');
		$friendid  = JRequest::getVar('friend', 0, 'get');
		$user  	   = JFactory::getUser();
		$db        = JFactory::getDbo();

		//validate the pending status...
		$db->setQuery('select status from #__tutolk_friends where friendid = '.$friendid.' and userid = '.$userid);
		
		if($db->loadResult() == 0) {

			$db->setQuery('update #__tutolk_friends set status = 1 where friendid = '.$friendid.' and userid = '.$userid);
			if($db->query()) {
				$db->setQuery('select status from #__tutolk_friends where friendid = '.$userid.' and userid = '.$friendid);
				if($db->loadResult() == 0) {
					$db->setQuery('update #__tutolk_friends set status = 1 where friendid = '.$userid.' and userid = '.$friendid);
					if($db->query()) {
						$msg = JText::_('COM_TUTOLK_FRIENDSHIP_VALIDATE_SUCCESS');
						$type = 'message';
					}
				}
			} else {
				$msg = JText::_('COM_TUTOLK_FRIENDSHIP_VALIDATE_ERROR');
				$type = 'error';
			}
		} else {
			$msg = JText::_('COM_TUTOLK_FRIENDSHIP_VALIDATE_NO_PENDING');
			$type = 'error';
		}
		
		$this->setRedirect(JRoute::_('index.php?option=com_tutolk&view=users&Itemid=110'), $msg, $type);
	}

	public function rating() {

		$db 		= JFactory::getDbo();
		$user 		= JFactory::getUser();
		$value 		= JRequest::getVar('value', 1, 'get');
		$userid 	= JRequest::getVar('userid', 0, 'get');
		$response	= array();

		$db->setQuery('select id from #__tutolk_rating where user_from = '.$user->id.' and user_to = '.$userid);
		if($id = $db->loadResult()) {
			$db->setQuery('update #__tutolk_rating set rate = '.$value.' where id = '.$id);
			$response['result'] = $db->query();
		} else {
			$rate = new stdClass();
			$rate->rate = $value;
			$rate->user_from = $user->id;
			$rate->user_to = $userid;
			$response['result'] = $db->insertObject('#__tutolk_rating', $rate);
		}
		echo json_encode($response);
	}

}
