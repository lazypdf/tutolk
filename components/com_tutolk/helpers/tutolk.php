<?php

/**
 * @version     1.0.0
 * @package     com_tutolk
 * @copyright   Copyright (C) 2015. Todos los derechos reservados.
 * @license     Licencia Pública General GNU versión 2 o posterior. Consulte LICENSE.txt
 * @author      aficat <kim@aficat.com> - http://www.afi.cat
 */
defined('_JEXEC') or die;

class TutolkHelper {
    
	/**
	 * return int the friendship status
	**/	
	public static function friendshipStatus($userid) {
			
		$user = JFactory::getUser();	
		$db   = JFactory::getDbo();

		$db->setQuery('select status from #__tutolk_friends where userid = '.$user->id.' and friendid = '.$userid);
		return $db->loadResult();
	}

	/**
	 * Simple log
	 * @param string $comment  The comment to log
	*/
	public static function addLogEntry($comment)
	{
		jimport('joomla.error.log');
		JLog::addLogger(array('text_file' => 'afi.log.php','text_entry_format' => '{DATETIME} {MESSAGE}{CLIENTIP}'),
	       	JLog::ALL & ~JLog::DEBUG,
	       	array('com_tutolk')); 
		JLog::add($comment, $path);
	}

	public static function getMostPopular() {
				
		$db   = JFactory::getDbo();
		$user = JFactory::getUser();

		$db->setQuery('select distinct(userid) from #__tutolk_users as u inner join #__tutolk_rating as r on r.user_to = u.userid where u.userid != '.$user->id);
		$rows = $db->loadObjectList();

		$i = 0;

		foreach($rows as $row) {

			$result[$i]['rating'] = TutolkHelper::getAllRating($row->userid);
			$result[$i]['userid'] = $row->userid;
			$i++;
		}

		arsort($result);
		array_slice($result, 0, 3);

		return $result;
	}

	public static function isOnline($userid) {
		if($userid == "") { return false; }
		$db = JFactory::getDbo();
		$db->setQuery('select count(userid) from #__session where userid = '.$userid);
		if($db->loadResult() > 0) { return true; } else { return false; }
	}

	public static function isMeetingOwner($id) {
		if($id == "") { return true; }
		$user = JFactory::getUser();
		$db = JFactory::getDbo();
		$db->setQuery('select userid from #__tutolk_meetings where id = '.$id);
		if($db->loadResult() == $user->id) { return true; } else { return false; }
	}

	public static function getModeratorJoinUrl($meetingID) {

		$db = JFactory::getDbo();
		jimport('bbb.bbb-api');
		$bbb = new BigBlueButton();

		$prefix = 'tutolk';

		$db->setQuery("SELECT * FROM #__tutolk_meetings WHERE id = ".$db->quote($meetingID));
		$row = $db->loadObject();

		$joinParams = array(
			'meetingId' => $prefix.$meetingID, 	// REQUIRED - We have to know which meeting to join.
			'username' => JFactory::getUser()->username,	// REQUIRED - The user name show in the BBB meeting.
			'password' => $row->moderatorPW, // REQUIRED - Must match either attendee pass for meeting.
			//'createTime' => $time, 	// OPTIONAL - string
			'userId' => JFactory::getUser()->id,	// OPTIONAL - string
			'webVoiceConf' => ''		// OPTIONAL - string
		);

		return $bbb->getJoinMeetingURL($joinParams);
	}
	
	public static function getAttendeeJoinUrl($meetingID) {

		$db = JFactory::getDbo();
		jimport('bbb.bbb-api');
		$bbb = new BigBlueButton();

		$prefix = 'tutolk';

		$db->setQuery("SELECT * FROM #__tutolk_meetings WHERE id = ".$db->quote($meetingID));
		$row = $db->loadObject();

		$joinParams = array(
			'meetingId' => $prefix.$meetingID, 	// REQUIRED - We have to know which meeting to join.
			'username' => JFactory::getUser()->username,	// REQUIRED - The user name show in the BBB meeting.
			'password' => $row->attendeePW, // REQUIRED - Must match either attendee pass for meeting.
			//'createTime' => $time, 	// OPTIONAL - string
			'userId' => JFactory::getUser()->id,	// OPTIONAL - string
			'webVoiceConf' => ''		// OPTIONAL - string
		);

		return $bbb->getJoinMeetingURL($joinParams);
	}
	
	public static function getEndMeetingUrl($meetingID) {
	
		$db = JFactory::getDbo();
		jimport('bbb.bbb-api');
		$bbb = new BigBlueButton();

		$prefix = 'tutolk';
		
		$db->setQuery("SELECT moderatorPW FROM #__tutolk_meetings WHERE id = ".$db->quote($meetingID));
		$row = $db->loadObject();

		$endParams = array (
			'meetingId' => $prefix.$meetingID, // REQUIRED - The unique id for the meeting
			'password' => $row->moderatorPW	// REQUIRED - The moderator password for the meeting
		);

		return $bbb->getEndMeetingURL($endParams);
	}

	public static function countFriends($userid) {

		$user = JFactory::getUser();
		$db   = JFactory::getDbo();

		$db->setQuery('select count(friendid) from #__tutolk_friends where userid = '.$userid.' and status = 1');
		return $db->loadResult();
	}

	public static function getUserData($userid, $field, $default='') {

		$db   = JFactory::getDbo();

		$db->setQuery("select $field from #__tutolk_users where userid = ".$userid);
		$response = $db->loadResult();
        	if($response == '' && $default != '') { return $default; }
        	return $response;
	}
	
	public static function getUserImage($userid=null) {

		$db   = JFactory::getDbo();
		$user = JFactory::getUser();
		
		if($userid == null) { $userid = $user->id; }
		
		if($userid == 0) { return 'images/users/tutolk.png'; }
		
		//check gravatar
		$db->setQuery("select gravatar from #__tutolk_user_params where userid = ".$userid);
		if($db->loadResult() == 1) {

			$userEmail = JFactory::getUser($userid)->id;
			$size = "150";
			$defaultAvatar = "http://www.tutolk.com/images/users/NoUser.png";
			$userEmail = strtolower($userEmail);
			$userEmailHash = md5($userEmail);
			$defaultAvatar = urlencode($defaultAvatar); 
			return "https://gravatar.com/avatar/$userEmailHash?d=$defaultAvatar&s=$size";
			
		} else {
			//check own image
			$db->setQuery("select image from #__tutolk_users where userid = ".$userid);
			$image = $db->loadResult();
			if($image != '') {
				return 'images/users/'.$image;
			} else {
				return 'images/users/NoUser.png';
			}
		}
	}
	
	public static function setDate($datetime)
	{
    		$user = JFactory::getUser();
    		
    		$timezone = TutolkHelper::getUserData($user->id, 'timezone', 'UTC');

    		$time_object = new DateTime($datetime, new DateTimeZone('UTC'));
    		$time_object->setTimezone(new DateTimeZone($timezone));

    		$user_datetime = $time_object->format('Y-m-d H:i');

    		return $user_datetime;  //RETURN 0000-00-00 00:00:00  
	}
	
	public static function setHour($datetime, $userid)
	{  		
    		$user = JFactory::getUser();
    		
    		$timezone = TutolkHelper::getUserData($user->id, 'timezone', 'UTC');

    		$time_object = new DateTime($datetime, new DateTimeZone('UTC'));
    		$time_object->setTimezone(new DateTimeZone($timezone));

    		$user_datetime = $time_object->format('H:i');

    		return $user_datetime;  //RETURN 00:00 
	}
	
	public static function getUserSex($sex)  {
	
		switch($sex) {
			case 'm':
			$response = 'Male';
			break;
			
			case 'f':
			$response = 'Female';
			break;
			
			default:
			$response = '';
			break;		
		}
		return $response;
	}
	
	public static function getMeetingData($id, $field, $default='') {

		$db   = JFactory::getDbo();

		$db->setQuery("select $field from #__tutolk_meetings where id = ".$id);
		$response = $db->loadResult();
        	if($response == '' && $default != '') { return $default; }
        	return $response;
	}

	public static function getFriends() {

		$user = JFactory::getUser();
		$db   = JFactory::getDbo();

		$db->setQuery(  "select u.name as nom, f.friendid, u.image from #__tutolk_friends as f ".
				"inner join #__tutolk_users as u on f.friendid = u.userid ".
				"where f.status = 1 and f.userid = ".$user->id);
		return $db->loadObjectList();
	}

	public static function sendMessage($userid, $from, $subject, $body) {

		$db   = JFactory::getDbo();
		$msg = new stdClass();

		$msg->subject  = $subject;
		$msg->body     = $body;
		$msg->userid   = $userid;
		$msg->usr_from = $from;
		$msg->status   = 0;
		$msg->sendDate = date('Y-m-d H:i:s');

		$db->insertObject('#__tutolk_messages', $msg);
	}

	public static function sendEmail($subject, $body, $email) {

		$mail 		= JFactory::getMailer();
		$config 	= JFactory::getConfig();

		$fromname  	= $config->get('fromname');
		$mailfrom	= $config->get('mailfrom');	
	
		$sender[]  	= $fromname;
		$sender[]	= $mailfrom;

		$htmlbody = '<div style="width:100%!important;height:100%;background-color:#5ead68;"><table style="width:50%;padding:20px;margin: 0 auto;"><tr><td></td><td style="text-align:center;background-color:#fff;display:block!important;max-width:600px!important;margin:0"><img src="http://www.tutolk.com/images/tutolk_logo.png" alt="Tutolk" style="margin-top:10px;" /><div style="padding:30px;max-width:600px;margin:0"><table width="100%"><tr><td><p>'.$body.'</p><p>Thanks, have a lovely day.</p><p/><a href="http://twitter.com/tutolk">Follow @tutolk on Twitter</a></td></tr></table></div></td><td></td></tr></table></div>';	
		
	        $mail->setSender( $sender );
	        $mail->addRecipient( $email );
		//$mail->addReplyTo($mailfrom);
	        $mail->setSubject( $subject );
	        $mail->setBody( $htmlbody );
	        $mail->IsHTML(true);
		$mail->Send();			
	}

	public static function getAllRating($userid) {

		$db = JFactory::getDbo();
		$rating = 0;
		$db->setQuery('select rate from #__tutolk_rating where user_to = '.$userid);
		$rows = $db->loadObjectList();
		if(count($rows) > 0) {	
			foreach($rows as $row) {
				$rating += $row->rate;
			}
			return ($rating / count($rows));
		} else {
			return $rating;
		}
	}

	function distance($lat1, $lon1, $lat2, $lon2, $unit) {

	  $theta = $lon1 - $lon2;
	  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
	  $dist = acos($dist);
	  $dist = rad2deg($dist);
	  $miles = $dist * 60 * 1.1515;
	  $unit = strtoupper($unit);

	  if ($unit == "K") {
	    return ($miles * 1.609344);
	  } else if ($unit == "N") {
	      return ($miles * 0.8684);
	    } else {
		return $miles;
	      }
	}
}
