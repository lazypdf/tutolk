<?php

/**
 * @version     1.0.0
 * @package     com_tutolk
 * @copyright   Copyright (C) 2015. Todos los derechos reservados.
 * @license     Licencia Pública General GNU versión 2 o posterior. Consulte LICENSE.txt
 * @author      aficat <kim@aficat.com> - http://www.afi.cat
 */
// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modelitem');
jimport('joomla.event.dispatcher');

/**
 * Tutolk model.
 */
class TutolkModelUser extends JModelItem {

    /**
     * Method to get an object.
     *
     * @param	integer	The id of the object to get.
     *
     * @return	mixed	Object on success, false on failure.
     */
    public function getData($id = null) {

        $id = JFactory::getApplication()->input->get('id');

        $db = JFactory::getDbo();
	$db->setQuery('select * from #__tutolk_users where id = '.$id);

        return $db->loadObject();
    }

    public function getTable($type = 'User', $prefix = 'TutolkTable', $config = array()) {
        $this->addTablePath(JPATH_COMPONENT_ADMINISTRATOR . '/tables');
        return JTable::getInstance($type, $prefix, $config);
    }

    public function getFriends($userid) {

		$db   = JFactory::getDbo();

		$db->setQuery(  "select u.name as nom, u.id as friendid, u.userid as usrid, u.image from #__tutolk_friends as f ".
				"inner join #__tutolk_users as u on f.friendid = u.userid ".
				"where f.status = 1 and f.userid = ".$userid);
		return $db->loadObjectList();
	}

}
