<?php
/**
 * @version     1.0.0
 * @package     com_tutolk
 * @copyright   Copyright (C) 2015. Todos los derechos reservados.
 * @license     Licencia Pública General GNU versión 2 o posterior. Consulte LICENSE.txt
 * @author      aficat <kim@aficat.com> - http://www.afi.cat
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modelform');
jimport('joomla.event.dispatcher');

/**
 * Tutolk model.
 */
class TutolkModelMeeting extends JModelForm
{

    var $_item = null;

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @since	1.6
	 */
	protected function populateState()
	{
		$app = JFactory::getApplication('com_tutolk');

		// Load state from the request userState on edit or from the passed variable on default
		if (JFactory::getApplication()->input->get('layout') == 'edit') {
		    $id = JFactory::getApplication()->getUserState('com_tutolk.edit.meeting.id');
		} else {
		    $id = JFactory::getApplication()->input->get('id');
		    JFactory::getApplication()->setUserState('com_tutolk.edit.meeting.id', $id);
		}
		$this->setState('meeting.id', $id);

		// Load the parameters.
		$params = $app->getParams();
		$params_array = $params->toArray();
		if(isset($params_array['item_id'])){
		    $this->setState('meeting.id', $params_array['item_id']);
		}
		$this->setState('params', $params);

	}


	/**
	 * Method to get an ojbect.
	 *
	 * @param	integer	The id of the object to get.
	 *
	 * @return	mixed	Object on success, false on failure.
	 */
	public function getData()
	{
		$id = JFactory::getApplication()->input->get('id');
		if($id != '') {
		$db = JFactory::getDbo();
		$db->setQuery('select * from #__tutolk_meetings where id = '.$id);
		$this->_item = $db->loadObject();
		}
		return $this->_item;
	}

	public function getTable($type = 'Meeting', $prefix = 'TutolkTable', $config = array())
	{
        	$this->addTablePath(JPATH_COMPONENT_ADMINISTRATOR.'/tables');
        	return JTable::getInstance($type, $prefix, $config);
	}

	/**
	 * Method to get the profile form.
	 *
	 * The base form is loaded from XML
     *
	 * @param	array	$data		An optional array of data for the form to interogate.
	 * @param	boolean	$loadData	True if the form is to load its own data (default case), false if not.
	 * @return	JForm	A JForm object on success, false on failure
	 * @since	1.6
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Get the form.
		$form = $this->loadForm('com_tutolk.meeting', 'meeting', array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form)) {
			return false;
		}

		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return	mixed	The data for the form.
	 * @since	1.6
	 */
	protected function loadFormData()
	{
		$data = JFactory::getApplication()->getUserState('com_tutolk.edit.meeting.data', array());

		if (empty($data)) {
		    	$data = $this->getData();
		}

		return $data;
	}

	public function sendMeetingMessages($data, $id)
	{
		$attendees = explode(',', $data['attendees']);

		$subject = JText::_('COM_TUTOLK_MEETING_NOTICE_ATTENDEES_SUBJECT');
		$base    = 'http://www.tutolk.com/index.php?option=com_tutolk';

		foreach($attendees as $att) {

			$accept  = $base.'&task=acceptMeeting&id='.$id.'&me='.$att;
			$decline = $base.'&task=declineMeeting&id='.$id.'&me='.$att;

			$body	 = JText::sprintf('COM_TUTOLK_MEETING_NOTICE_ATTENDEES_BODY', TutolkHelper::getUserData($data['userid'], 'name'), $data['CreateDate'], $accept, $decline);

			TutolkHelper::sendMessage($att, 0, $subject, $body);
			//TutolkHelper::addLogEntry('Attendee '. $data['attendees']);
			TutolkHelper::sendEmail($subject, $body, JFactory::getUser($att)->email);
		}
	}

	public function noticeNewDate($row)
	{
		$attendees = explode(',', $row->attendees);

		$subject = JText::_('COM_TUTOLK_MEETING_NOTICE_NEW_DATE_SUBJECT');
		$base    = 'http://www.tutolk.com/index.php?option=com_tutolk';

		foreach($attendees as $att) {

			$accept  = $base.'&task=acceptMeeting&id='.$row->id.'&me='.$att;
			$decline = $base.'&task=declineMeeting&id='.$row->id.'&me='.$att;

			$body	 = JText::sprintf('COM_TUTOLK_MEETING_NOTICE_NEW_DATE_BODY', TutolkHelper::getUserData($row->userid, 'name'), $row->CreateDate, $accept, $decline);

			TutolkHelper::sendMessage($att, 0, $subject, $body);
			//TutolkHelper::addLogEntry('Attendee '. $att);
			TutolkHelper::sendEmail($subject, $body, JFactory::getUser($att)->email);
		}
	}

	public function noticeNewAttendees($row, $result)
	{
		$subject = JText::_('COM_TUTOLK_MEETING_NOTICE_ATTENDEES_SUBJECT');
		$base    = 'http://www.tutolk.com/index.php?option=com_tutolk';

		foreach($result as $att) {

			$accept  = $base.'&task=acceptMeeting&id='.$row->id.'&me='.$att;
			$decline = $base.'&task=declineMeeting&id='.$row->id.'&me='.$att;

			$body	 = JText::sprintf('COM_TUTOLK_MEETING_NOTICE_NEW_DATE_BODY', TutolkHelper::getUserData($row->userid, 'name'), $row->CreateDate, $accept, $decline);

			TutolkHelper::sendMessage($att, 0, $subject, $body);
			//TutolkHelper::addLogEntry('Attendee '. $data['attendees']);
			TutolkHelper::sendEmail($subject, $body, JFactory::getUser($att)->email);
		}
	}

	/**
	 * Method to save the form data.
	 *
	 * @param	array		The form data.
	 * @return	mixed		The user id on success, false on failure.
	 * @since	1.6
	 */
	public function store($data)
	{
		$row =& $this->getTable();

		$post_data = JRequest::get( 'post' );
   		$data      = $post_data["jform"];

        	$data['attendees'] = implode(',', $data['attendees']);
        	
        	//si es edición...
		if($data['id'] != '') {
			$db = JFactory::getDbo();
			$db->setQuery('select * from #__tutolk_meetings where id = '.$data['id']);
			$item = $db->loadObject();
		}

		//proceed...
		if (!$row->bind($data))
		{
		    $this->setError($this->_db->getErrorMsg());
		    return false;
		}

		if (!$row->check())
		{
		    $this->setError($this->_db->getErrorMsg());
		    return false;
		}

		if (!$row->store())
		{
		    $this->setError($this->_db->getErrorMsg());
		    return false;
		}

		//si es edición...
		if($data['id'] != '') {
			//si cambia hora avisar...
			if($data['CreateDate'] <> $item->CreateDate) {
				$this->noticeNewDate($item);
			}
			//si hay nuevos invitados enviar mensajes a estos...
			//si hay nuevos invitados enviar mensajes a estos...
			$old_attendees = implode(',', $item->attendees);
			$result = array_diff($data['attendees'], $old_attendees);
			if(count($result) > 0) {
				$this->noticeNewAttendees($item, $result);
			}

		} else {
			//send messages to new attendees...
			$this->sendMeetingMessages($data, $item->id);
		}

		return true;

	}

}

