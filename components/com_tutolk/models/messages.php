<?php

/**
 * @version     1.0.0
 * @package     com_tutolk
 * @copyright   Copyright (C) 2015. Todos los derechos reservados.
 * @license     Licencia Pública General GNU versión 2 o posterior. Consulte LICENSE.txt
 * @author      aficat <kim@aficat.com> - http://www.afi.cat
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of Tutolk records.
 */
class TutolkModelMessages extends JModelList
{

	/**
	 * Constructor.
	 *
	 * @param    array    An optional associative array of configuration settings.
	 *
	 * @see        JController
	 * @since      1.6
	 */
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(

			);
		}
		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @since    1.6
	 */
	protected function populateState($ordering = null, $direction = null)
	{

		// Initialise variables.
		$app = JFactory::getApplication();

        	// List state information
		//$value = $app->getUserStateFromRequest('global.list.limit', 'limit', $app->getCfg('list_limit'));
		$value = JRequest::getInt('limit', $app->getCfg('list_limit', 0));
		$this->setState('list.limit', $value);

		//$value = $app->getUserStateFromRequest($this->context.'.limitstart', 'limitstart', 0);
		$value = JRequest::getInt('limitstart', 0);
		$this->setState('list.start', $value);

		$orderCol = JRequest::getCmd('filter_orderby');
		if (!in_array($orderCol, $this->filter_fields)) {
			$orderCol = 'id';
		}
		$this->setState('list.ordering', $orderCol);

		$listOrder =  JRequest::getCmd('filter_order_Dir', 'ASC');
		if (!in_array(strtoupper($listOrder), array('ASC', 'DESC', ''))) {
			$listOrder = 'ASC';
		}
		$this->setState('list.direction', $listOrder);

		$search = $this->getUserStateFromRequest($this->context.'.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$status = $this->getUserStateFromRequest($this->context.'.filter.status', 'filter_status');
		$this->setState('filter.status', $status);
		
		$userid = $this->getUserStateFromRequest($this->context.'.filter.userid', 'filter_userid');
		$this->setState('filter.userid', $userid);

		// Load the parameters.
		$params = JComponentHelper::getParams('com_tutolk');
		$this->setState('params', $params);

		// List state information.
		$this->setState('layout', JRequest::getCmd('layout'));
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return    JDatabaseQuery
	 * @since    1.6
	 */
	protected function getListQuery() {

		$db	= $this->getDbo();
		$user   = JFactory::getUser();
		$query	= $db->getQuery(true);
		$query->select(
		        $this->getState(
		                'list.select', 'DISTINCT a.*'
		        )
		);
		$query->from('`#__tutolk_messages` AS a');

		$query->where('a.userid = '.$user->id);

		// Filter by search
		$search = $this->getState('filter.search');
		if($search != "") {
			$filter = $db->Quote('%'.$db->escape($search, true).'%', false);
			$query->where('(a.subject LIKE '.$filter.' OR a.body LIKE '.$filter.')');
		}

		// Filter by status
		$status = $this->getState('filter.status', '');
		if($status != "") {
			$query->where('a.status = '.$status);
		}
		
		// Filter by friend
		$userid = $this->getState('filter.userid', '');
		if($userid != "") {
			$query->where('a.usr_from = '.$userid);
		}

		// Add the list ordering clause.
		$orderby = $this->getState('filter.orderby', 'id');
		$query->order($this->getState('filter_orderby', $orderby). ' DESC');
		//echo $query;
		return $query;
	}

	public function getReplies($id)
	{

		$db = JFactory::getDbo();
		$db->setQuery('select * from #__tutolk_messages where reply_to = '.$id.' order by id asc');
		return $db->loadObjectList();
	}

	public function hasReplies($id)
	{

		$db = JFactory::getDbo();
		$db->setQuery('select count(id) from #__tutolk_messages where reply_to = '.$id);
		if($db->loadResult() > 0) { return true; } else { return false; }
	}

	public function getItems()
	{
		$items = parent::getItems();

		return $items;
	}


}

