<?php

/**
 * @version     1.0.0
 * @package     com_tutolk
 * @copyright   Copyright (C) 2015. Todos los derechos reservados.
 * @license     Licencia Pública General GNU versión 2 o posterior. Consulte LICENSE.txt
 * @author      aficat <kim@aficat.com> - http://www.afi.cat
 */
// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modelitem');
jimport('joomla.event.dispatcher');

/**
 * Tutolk model.
 */
class TutolkModelRating extends JModelItem {

	public function getRating($userid) {

		$db = JFactory::getDbo();
		$user = JFactory::getUser();
		$rating = 0;
		$db->setQuery('select rate from #__tutolk_rating where user_from = '.$user->id.' and user_to = '.$userid);
		if($db->loadResult() > 0) {
			$rating = $db->loadResult();
		}
		return $rating;
	}

}
