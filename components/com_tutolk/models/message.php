<?php
/**
 * @version     1.0.0
 * @package     com_tutolk
 * @copyright   Copyright (C) 2015. Todos los derechos reservados.
 * @license     Licencia Pública General GNU versión 2 o posterior. Consulte LICENSE.txt
 * @author      aficat <kim@aficat.com> - http://www.afi.cat
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modelform');
jimport('joomla.event.dispatcher');

/**
 * Tutolk model.
 */
class TutolkModelMessage extends JModelForm
{
    
    var $_item = null;
    
	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @since	1.6
	 */
	protected function populateState()
	{
		$app = JFactory::getApplication('com_tutolk');

		// Load state from the request userState on edit or from the passed variable on default
		if (JFactory::getApplication()->input->get('layout') == 'edit') {
		    $id = JFactory::getApplication()->getUserState('com_tutolk.edit.message.id');
		} else {
		    $id = JFactory::getApplication()->input->get('id');
		    JFactory::getApplication()->setUserState('com_tutolk.edit.message.id', $id);
		}
		$this->setState('message.id', $id);

		// Load the parameters.
		$params = $app->getParams();
		$params_array = $params->toArray();
		if(isset($params_array['item_id'])){
		    $this->setState('message.id', $params_array['item_id']);
		}
		$this->setState('params', $params);

	}

	public function getMessageSubject($id) 
	{
		$db = JFactory::getDbo();
		$db->setQuery('select subject from #__tutolk_messages where id = '.$id);
		return $db->loadResult();
	}
        

	/**
	 * Method to get an ojbect.
	 *
	 * @param	integer	The id of the object to get.
	 *
	 * @return	mixed	Object on success, false on failure.
	 */
	public function getData($id = null)
	{
		if ($this->_item === null)
		{
			$db = JFactory::getDbo();
			$db->setQuery('select * from #__tutolk_messages where userid = '.JFactory::getUser()->id);
			$this->_item = $db->loadObject();
		}

		return $this->_item;
	}
    
	public function getTable($type = 'Message', $prefix = 'TutolkTable', $config = array())
	{   
        	$this->addTablePath(JPATH_COMPONENT_ADMINISTRATOR.'/tables');
        	return JTable::getInstance($type, $prefix, $config);
	}       
    
	/**
	 * Method to get the profile form.
	 *
	 * The base form is loaded from XML 
     * 
	 * @param	array	$data		An optional array of data for the form to interogate.
	 * @param	boolean	$loadData	True if the form is to load its own data (default case), false if not.
	 * @return	JForm	A JForm object on success, false on failure
	 * @since	1.6
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Get the form.
		$form = $this->loadForm('com_tutolk.message', 'message', array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form)) {
			return false;
		}

		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return	mixed	The data for the form.
	 * @since	1.6
	 */
	protected function loadFormData()
	{
		$data = JFactory::getApplication()->getUserState('com_tutolk.edit.message.data', array());
		if (empty($data)) {
		    $data = $this->getData();
		}
		
		return $data;
	}

	/**
	 * Method to save the form data.
	 *
	 * @param	array		The form data.
	 * @return	mixed		The user id on success, false on failure.
	 * @since	1.6
	 */
	public function store($data)
	{
		$row =& $this->getTable();	
		
		//proceed...
		if (!$row->bind($data))
		{
		    $this->setError($this->_db->getErrorMsg());
		    return false;
		}

		if (!$row->check())
		{
		    $this->setError($this->_db->getErrorMsg());
		    return false;
		}
	
		if (!$row->store())
		{
		    $this->setError($this->_db->getErrorMsg());
		    return false;
		}
		
		return true;
        
	}
        
}
