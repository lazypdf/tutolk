<?php

/**
 * @version     1.0.0
 * @package     com_tutolk
 * @copyright   Copyright (C) 2015. Todos los derechos reservados.
 * @license     Licencia Pública General GNU versión 2 o posterior. Consulte LICENSE.txt
 * @author      aficat <kim@aficat.com> - http://www.afi.cat
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of Tutolk records.
 */
class TutolkModelUsers extends JModelList
{

	/**
	 * Constructor.
	 *
	 * @param    array    An optional associative array of configuration settings.
	 *
	 * @see        JController
	 * @since      1.6
	 */
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				
			);
		}
		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @since    1.6
	 */
	protected function populateState($ordering = null, $direction = null)
	{

		// Initialise variables.
		$app = JFactory::getApplication();
                
        	// List state information
		//$value = $app->getUserStateFromRequest('global.list.limit', 'limit', $app->getCfg('list_limit'));
		$value = JRequest::getInt('limit', $app->getCfg('list_limit', 0));
		$this->setState('list.limit', $value);

		//$value = $app->getUserStateFromRequest($this->context.'.limitstart', 'limitstart', 0);
		$value = JRequest::getInt('limitstart', 0);
		$this->setState('list.start', $value);

		$orderCol = JRequest::getCmd('filter_orderby');
		if (!in_array($orderCol, $this->filter_fields)) {
			$orderCol = 'id';
		}
		$this->setState('list.ordering', $orderCol);

		$listOrder =  JRequest::getCmd('filter_order_Dir', 'ASC');
		if (!in_array(strtoupper($listOrder), array('ASC', 'DESC', ''))) {
			$listOrder = 'ASC';
		}
		$this->setState('list.direction', $listOrder);
		
		$search = $this->getUserStateFromRequest($this->context.'.filter.search', 'filter_search');
		$this->setState('filter.search', $search);
		
		$age = $this->getUserStateFromRequest($this->context.'.filter.age', 'filter_age');
		$this->setState('filter.age', $age);

		$sex = $this->getUserStateFromRequest($this->context.'.filter.sex', 'filter_sex');
		$this->setState('filter.sex', $sex);
		
		$country = $this->getUserStateFromRequest($this->context.'.filter.country', 'filter_country');
		$this->setState('filter.country', $country);

		$orderby = $this->getUserStateFromRequest($this->context.'.filter.orderby', 'filter_orderby');
		$this->setState('filter.orderby', $orderby);

		$webcam = $this->getUserStateFromRequest($this->context.'.filter.webcam', 'filter_webcam');
		$this->setState('filter.webcam', $webcam);

		$online = $this->getUserStateFromRequest($this->context.'.filter.online', 'filter_online');
		$this->setState('filter.online', $online);

		// Load the parameters.
		$params = JComponentHelper::getParams('com_tutolk');
		$this->setState('params', $params);

		// List state information.
		$this->setState('layout', JRequest::getCmd('layout'));
	}

	public function getRandom() {
		
		$db = JFactory::getDbo();
		$user   = JFactory::getUser();
		$session = JFactory::getSession();

		$query  = 'SELECT DISTINCT a.* FROM #__tutolk_users as a ';
		
		//no repeat users...
		$userslist = $session->get('userslist');
		if(empty($userslist)) { 
			$userslist[] = $user->id; 
		}
			
		$query .= 'WHERE a.userid NOT IN ( ' . implode($userslist, ',') . ' ) ';

		$query .= 'AND status = 1 ';

		// Filter by tags
		$tags = JRequest::getVar('tags');
		if($tags != "") {
			$session->set('tags', $tags);
			$tag = explode(',', $tags);
			$i = 0;
			foreach($tag as $k) {
				if($i == 0) { $query .= ' AND ('; } else { $query .= ' OR'; }
				$query .= ' (FIND_IN_SET('.$db->Quote($tag[$i]).', LOWER(tags)) > 0) ';
				$i++;
			}
			$query .= ') ';
		} else {
			$session->set('tags', '');
		}

		// Filter by age
		$age = $this->getState('filter.age', '0;99');
		$ages = explode(';', $age);
		if($ages[0] > 0 || $ages[1] < 99) {
			$query .= 'OR a.userid NOT IN ( ' . implode($userslist, ',') . ' ) AND (a.age BETWEEN '.$ages[0].' AND '.$ages[1].') ';
		}

		// Filter by sex
		$sex = $this->getState('filter.sex');
		if($sex != "") {
			$filter = $db->Quote($sex);
			$query .= 'OR a.userid NOT IN ( ' . implode($userslist, ',') . ' ) AND a.sex = '.$filter.' ';
		}
		
		$query .= 'ORDER BY a.ordering DESC LIMIT 5';
		$db->setQuery($query);
		//echo $query;
		return $db->loadObjectList();
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return    JDatabaseQuery
	 * @since    1.6
	 */
	protected function getListQuery() {
		$db	= $this->getDbo();
		$user   = JFactory::getUser();
		$query	= $db->getQuery(true);	
		$query->select(
		        $this->getState(
		                'list.select', 'DISTINCT a.*'
		        )
		);
		$query->from('`#__tutolk_users` AS a');        

		$query->where('a.userid != '.$user->id);

		$query->where('a.status = 1');

		// Filter by search
		$search = $this->getState('filter.search');
		if($search != "") {
			$filter = $db->Quote('%'.$db->escape($search, true).'%', false);
			$query->where('(a.name LIKE '.$filter.' OR a.short_description LIKE '.$filter.')');
		}

		// Filter by age
		$age = $this->getState('filter.age', '0;99');
		$ages = explode(';', $age);
		if($ages[0] > 0 && $ages[1] < 99) {
			$query->where('(a.age BETWEEN '.$ages[0].' AND '.$ages[1].')');
		}

		// Filter by sex
		$sex = $this->getState('filter.sex');
		if($sex != "") {
			$filter = $db->Quote($sex);
			$query->where('a.sex = '.$filter);
		}

		// Filter by tags
		$session = JFactory::getSession();
		$tags = JRequest::getVar('tags');
		if($tags != "") {
			$session->set('tags', $tags);
			$tag = explode(',', $tags);
			$i = 0;
			foreach($tag as $k) {
				$query->where('FIND_IN_SET('.$db->Quote($tag[$i]).', LOWER(tags)) > 0 ');
				$i++;
			}
			
		} else {
			$session->set('tags', '');
		}

		// Filter by country
		$country = $this->getState('filter.country');
		if($country != "") {
			$filter = $db->Quote($country);
			$query->where('a.country = '.$filter);
		}

		// Filter by webcam
		$webcam = $this->getState('filter.webcam');
		if($webcam != "") {
			$query->where('a.webcam = '.$webcam);
		}

		// Filter by online
		$online = $this->getState('filter.online');
		if($online != "") {
			if($online == 1) {
				$query->join('inner', '#__session as s on s.userid = a.userid');
			} else {
				$query->join('inner', '#__session as s on s.userid != a.userid');
			}
		}

		// Add the list ordering clause.
		$orderby = $this->getState('filter.orderby', 'ordering');
		$query->order($this->getState('filter_orderby', $orderby). ' DESC');
		//echo $query;
		return $query;
	}

	public function setOrderingByTags($userid, $tags)
	{
		$db = JFactory::getDbo();
		$db->setQuery('select tags from #__tutolk_users where userid = '.$userid);
		$usertags = explode(',', $db->loadResult());
		$result = count(array_intersect($usertags, explode(',',$tags)));
		$user = new stdClass();
 		$user->userid = $userid;
 		$user->ordering = $result;
		$db->updateObject('#__tutolk_users', $user, 'userid', false);		
	}

	public function getItems()
	{
		$items = parent::getItems();	

		return $items;
	}

	/**
	 * Overrides the default function to check Date fields format, identified by
	 * "_dateformat" suffix, and erases the field if it's not correct.
	 */
	protected function loadFormData()
	{
		$app              = JFactory::getApplication();
		$filters          = $app->getUserState($this->context . '.filter', array());
		$error_dateformat = false;
		foreach ($filters as $key => $value)
		{
			if (strpos($key, '_dateformat') && !empty($value) && !$this->isValidDate($value))
			{
				$filters[$key]    = '';
				$error_dateformat = true;
			}
		}
		if ($error_dateformat)
		{
			$app->enqueueMessage(JText::_("COM_TUTOLK_SEARCH_FILTER_DATE_FORMAT"), "warning");
			$app->setUserState($this->context . '.filter', $filters);
		}

		return parent::loadFormData();
	}

	// get tags
	public function getTags() 
	{
		$response = array();
		$db	= JFactory::getDbo();
		$query = "SELECT GROUP_CONCAT(tags SEPARATOR ',') FROM #__tutolk_users";
		$db->setQuery($query);
		$results = explode(',', $db->loadResult());
		foreach($results as $result) {
			if(!in_array($result, $response)) {
				$response[] = $result;
			}
		}
		return json_encode($response);
	}

	/**
	 * Checks if a given date is valid and in an specified format (YYYY-MM-DD)
	 *
	 * @param string Contains the date to be checked
	 *
	 */
	private function isValidDate($date)
	{
		return preg_match("/^(19|20)\d\d[-](0[1-9]|1[012])[-](0[1-9]|[12][0-9]|3[01])$/", $date) && date_create($date);
	}

}
