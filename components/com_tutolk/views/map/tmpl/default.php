<?php
/**
 * @version     1.0.0
 * @package     com_tutolk
 * @copyright   Copyright (C) 2015. Todos los derechos reservados.
 * @license     Licencia Pública General GNU versión 2 o posterior. Consulte LICENSE.txt
 * @author      aficat <kim@aficat.com> - http://www.afi.cat
 */
// no direct access
defined('_JEXEC') or die;

$app  = JFactory::getApplication();
$user = JFactory::getUser();
//$model = $this->getModel();
//redirect guests users to login/register area
if($user->guest) {
	$returnurl = JRoute::_('index.php?option=com_users&view=login&return='.base64_encode(JUri::current()), false);
    	$app->redirect($returnurl, JText::_('COM_TUTOLK_LOGIN_BEFORE'));
}

?>

<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <style type="text/css">
      html { height: 100% }
      body { height: 100%; margin: 0; padding: 0 }
      #map_canvas { height: 100% }
    </style>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCh2iuwQCW-GFFWfhntd95LKfAAGH_4bAc" type="text/javascript"></script>
    <script type="text/javascript">
      var map;
      function initialize() {
        var mapOptions = {
          center: new google.maps.LatLng(58, 16),
          zoom: 5,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        map = new google.maps.Map(document.getElementById("map_canvas"),
            mapOptions);
      }
    </script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>    
  </head>
  <body onload="initialize()">
    <div id="map_canvas" style="width:100%; height:100%"></div>
    <script type="text/javascript">
      $(document).ready(function() {
        $.getJSON("http://www.tutolk.com/index.php?option=com_tutolk&task=getMapJson&tmpl=raw", function(json1) {
          $.each(json1, function(key, results) {
            var latLng = new google.maps.LatLng(results.latitude, results.longitude);
		var id = results.id;
		//console.log(results.id);
		if(results.image != '') { var image = results.image; } else { var image = 'NoUser.png'; }
          	var marker = new google.maps.Marker({
            		position: latLng,
            		map: map,
	    		shape:{coords:[17,17,18],type:'circle'},
	    		icon:{url:'http://www.tutolk.com/images/users/'+image,
            		scaledSize: new google.maps.Size(50, 50)}, // scaled size
  	    		optimized:true,
	    		url: "http://www.tutolk.com/index.php/profile/user/"+id
          	});
		google.maps.event.addListener(marker, 'click', function() {
        		window.location.href = this.url;
    		});
            marker.setMap(map);
          });
        });
      });
    </script>
  </body>
</html>

