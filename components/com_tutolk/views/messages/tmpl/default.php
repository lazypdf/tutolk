<?php

/**
 * @version     1.0.0
 * @package     com_tutolk
 * @copyright   Copyright (C) 2015. Todos los derechos reservados.
 * @license     Licencia Pública General GNU versión 2 o posterior. Consulte LICENSE.txt
 * @author      aficat <kim@aficat.com> - http://www.afi.cat
 */
// No direct access
defined('_JEXEC') or die;
$app  = JFactory::getApplication();
$user = JFactory::getUser();
$model = $this->getModel();
//redirect guests users to login/register area
if($user->guest) {
	$returnurl = JRoute::_('index.php?option=com_users&view=login&return='.base64_encode(JUri::current()), false);
    	$app->redirect($returnurl, JText::_('COM_TUTOLK_LOGIN_BEFORE'));
}

JFactory::getDocument()->addStyleSheet('components/com_tutolk/assets/css/messages.css');

$listOrder	= $this->escape($this->state->get('list.ordering'));
$listDirn	= $this->escape($this->state->get('list.direction'));

?>

<section id="list">

<div class="col-md-6 pull-left">
	<h1>Messages</h1>
</div>
<div class="col-md-6 pull-right" style="text-align:right;">
        <a href="<?php echo JRoute::_('index.php?option=com_tutolk&view=users'); ?>" class="btn btn-labeled btn-success">
		<span class="btn-label"><i class="fa fa-chevron-left"></i></span>Back
	</a>
	<a href="<?php echo JRoute::_('index.php?option=com_tutolk&view=message'); ?>" class="btn btn-labeled btn-success">
		<span class="btn-label"><i class="fa fa-plus"></i></span>New
	</a>
	<a href="<?php echo JRoute::_('index.php?option=com_tutolk&task=message.markAllRead&'.JSession::getFormToken().'=1'); ?>" class="btn btn-labeled btn-success">
		<span class="btn-label"><i class="fa fa-check-circle"></i></span>Mark all as read
	</a>
</div>

<div class="clearfix"></div>

<!-- left col -->
<div class="col-md-12">

	<!-- filters -->
	<form id="adminForm" name="adminForm" action="" method="get" class="form-inline">
	<div class="filters-advanced">
        		<div id="filter-panel" class="filter-panel">
            			<div class="panel panel-default">
                			<div class="panel-body">
        					<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
							<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />

							<div class="form-group" style="margin-right:20px;">
						    	<input type="text" placeholder="Search" class="form-control" name="filter_search" id="filter_search" value="<?php echo $this->state->get('filter.search'); ?>" onchange="this.form.submit()">
							</div><!-- form group [search] -->

							<div class="form-group">
						    	<select id="filter_status" name="filter_status" class="form-control" onchange="this.form.submit()">
									<option <?php if($this->state->get('filter.status') == '') : ?>selected<?php endif; ?> value="">Status</option>
									<option <?php if($this->state->get('filter.status') == '0') : ?>selected<?php endif; ?> value="0">Unread</option>
						        	<option <?php if($this->state->get('filter.status') == '1') : ?>selected<?php endif; ?> value="1">Read</option>
						    	</select>
							</div> <!-- form group [status] -->
						
							<div class="form-group">    
						    	<select name="filter_userid" id="filter_userid" class="form-control" onchange="this.form.submit()">
						    		<option <?php if($this->state->get('filter.userid') == '') : ?>selected<?php endif; ?> value="">Select a friend</option>
						    		<option <?php if($this->state->get('filter.userid') == '0') : ?>selected<?php endif; ?> value="0">System messages</option>
									<?php foreach(TutolkHelper::getFriends() as $friend) : ?>
									<option <?php if($this->state->get('filter.userid') == $friend->friendid) : ?>selected<?php endif; ?>  value="<?php echo $friend->friendid; ?>"><?php echo $friend->nom; ?></option>
									<?php endforeach; ?>
								</select>
							</div> <!-- form group [friends] -->

            			</div>
			</div>
	</div>
	<!-- end filters -->

	<!-- start messages -->
	<div class="box box-success direct-chat direct-chat-success">
		<div class="box-body">
          		<!-- Conversations are loaded here -->
          		<div class="direct-chat-messages">       
		<?php if(count($this->items)) : ?>
		  <?php
		  $i = 1;
		  foreach($this->items as $item) :
			  if($item->usr_from == 0) {
				$from_name = 'Tutolk';
			  } else {
				$from_name = JFactory::getUser($item->usr_from)->username;
			  }
		  ?>
		  
		  <!-- Message left -->
		<div class="direct-chat-msg">
			<div class="direct-chat-info clearfix">
				<span class="direct-chat-name pull-left"><?php echo $from_name; ?></span>
				<span class="direct-chat-timestamp pull-right localize"><?php echo TutolkHelper::setDate($item->sendDate); ?></span>
			</div>
			<!-- /.direct-chat-info -->
              <img class="direct-chat-img" src="<?php echo TutolkHelper::getUserImage($item->usr_from); ?>" alt="">
              <div class="direct-chat-text <?php if($item->status == 0): ?>unread<?php endif; ?>">
                <?php echo $item->subject.'<br/>'.$item->body; ?>
                <div class="direct-chat-buttons">
                <?php if($item->usr_from != 0) : ?>
				<a href="index.php?option=com_tutolk&view=message&to=<?php echo $item->usr_from; ?>&Itemid=130">
					<span class="fa-stack">
                                		<i class="fa fa-square fa-stack-2x"></i>
                                		<i class="fa fa-reply fa-stack-1x fa-inverse"></i>
                            		</span>
				</a>
				<?php endif; ?>
				<?php if($item->status == 0) : ?>
				<a href="<?php echo JRoute::_('index.php?option=com_tutolk&task=message.readMessage&id='. $item->id.'&'.JSession::getFormToken().'=1'); ?>">
					<span class="fa-stack">
                                		<i class="fa fa-square fa-stack-2x"></i>
                                		<i class="fa fa-check-circle fa-stack-1x fa-inverse"></i>
                            		</span>
				</a>
				<?php endif; ?>
				<?php if($item->status == 1) : ?>
				<a href="<?php echo JRoute::_('index.php?option=com_tutolk&task=message.unreadMessage&id='. $item->id.'&'.JSession::getFormToken().'=1'); ?>">
					<span class="fa-stack">
                                		<i class="fa fa-square fa-stack-2x"></i>
                                		<i class="fa fa-times fa-stack-1x fa-inverse"></i>
                            		</span>
				</a>
				<?php endif; ?>
				<a href="<?php echo JRoute::_('index.php?option=com_tutolk&task=message.deleteMessage&id='. $item->id.'&'.JSession::getFormToken().'=1'); ?>1">
					<span class="fa-stack">
                                		<i class="fa fa-square fa-stack-2x"></i>
                                		<i class="fa fa-trash-o fa-stack-1x fa-inverse"></i>
                            		</span>
				</a>
              </div>
              </div>      
            </div>
	<!-- end message -->

	  <?php
	  $i++;
	  endforeach; ?>
	<?php endif; ?>
	</div>
    </div>
</div>
</form>
</div>
<!-- end col -->

<p class="counter">
	<?php echo $this->pagination->getPagesCounter(); ?>
</p>
<?php echo $this->pagination->getPagesLinks(); ?>
</section>

