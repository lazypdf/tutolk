<?php
/**
 * @version     1.0.0
 * @package     com_tutolk
 * @copyright   Copyright (C) 2015. Todos los derechos reservados.
 * @license     Licencia Pública General GNU versión 2 o posterior. Consulte LICENSE.txt
 * @author      aficat <kim@aficat.com> - http://www.afi.cat
 */
// no direct access
defined('_JEXEC') or die;
error_reporting(E_ALL ^ E_NOTICE);
$app  = JFactory::getApplication();
$user = JFactory::getUser();
$model = $this->getModel();
//redirect guests users to login/register area
if($user->guest) {
	$returnurl = JRoute::_('index.php?option=com_users&view=login&return='.base64_encode(JUri::current()), false);
    	$app->redirect($returnurl, JText::_('COM_TUTOLK_LOGIN_BEFORE'));
}
$doc = JFactory::getDocument();
JHtml::_('behavior.keepalive');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
$userid = JRequest::getVar('to', '', 'get');

?>

<section id="list">

<div class="col-md-6 pull-left">
	<h1>
		Compose 
		<?php if($userid != '') : ?>
		message for <?php echo TutolkHelper::getUserData($userid, 'name'); ?>
		<?php endif; ?>
	</h1>
</div>
<div class="col-md-6 pull-right" style="text-align:right;">
        <a href="<?php echo JRoute::_('index.php?option=com_tutolk&view=messages'); ?>" class="btn btn-labeled btn-success">
		<span class="btn-label"><i class="fa fa-chevron-left"></i></span>Back
	</a>
	<a onclick="Compose.submit();" class="btn btn-labeled btn-success save">
		<span class="btn-label"><i class="fa fa-floppy-o"></i></span>Send
	</a>						
</div>

<div class="clearfix"></div>

<!-- left col -->
<div class="col-md-12">

<form id="Compose" name="Compose" action="<?php echo JRoute::_('index.php?option=com_tutolk&task=message.save'); ?>" method="post" class="form-validate form-vertical" enctype="multipart/form-data">
	<input type="hidden" name="option" value="com_tutolk" />
        <input type="hidden" name="task" value="message.save" />
        <?php echo JHtml::_('form.token'); ?>
	<?php echo $this->form->getInput('usr_from', '', $user->id); ?>
	<?php echo $this->form->getInput('status', '', 0); ?>
	<?php echo $this->form->getInput('sendDate', '', date('Y-m-d H:i:s')); ?>

	<div class="col-md-8">

		    
			<div class="desc">
				<select name="jform[userid]" id="jform_userid">
					<?php foreach(TutolkHelper::getFriends() as $friend) : ?>
					<option <?php if($friend->friendid == $userid): ?>selected<?php endif; ?> value="<?php echo $friend->friendid; ?>"><?php echo $friend->nom; ?></option>
					<?php endforeach; ?>
				</select>
		    	</div>
		    
                    <div class="desc"><?php echo $this->form->getInput('subject', '', ''); ?></div>
                    <div class="desc"><?php echo $this->form->getInput('body', '', ''); ?></div>

	</div>
</form>
</div>
<!-- end col -->

</section>



