<?php
/**
 * @version     1.0.0
 * @package     com_tutolk
 * @copyright   Copyright (C) 2015. Todos los derechos reservados.
 * @license     Licencia Pública General GNU versión 2 o posterior. Consulte LICENSE.txt
 * @author      aficat <kim@aficat.com> - http://www.afi.cat
 */
// no direct access
defined('_JEXEC') or die;
error_reporting(E_ALL ^ E_NOTICE);
$app  = JFactory::getApplication();
$user = JFactory::getUser();
$model = $this->getModel();
//redirect guests users to login/register area
if($user->guest) {
	$returnurl = JRoute::_('index.php?option=com_users&view=login&return='.base64_encode(JUri::current()), false);
    	$app->redirect($returnurl, JText::_('COM_TUTOLK_LOGIN_BEFORE'));
}
$doc = JFactory::getDocument();
JHtml::_('behavior.keepalive');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');

$doc->addStylesheet('templates/tutolk/css/bootstrap-tags.css');
$doc->addStylesheet('templates/tutolk/css/tokenfield-typeahead.css');
$doc->addScript('templates/tutolk/js/bootstrap-tags.js');
$doc->addScript('templates/tutolk/js/typeahead.bundle.min.js');
$doc->addStylesheet('templates/tutolk/css/crop.css');
$doc->addStylesheet('templates/tutolk/css/bootstrap-switch.min.css');
$doc->addScript('templates/tutolk/js/crop.js');
$doc->addScript('templates/tutolk/js/jquery.geolocation.edit.min.js');
$doc->addScript('templates/tutolk/js/bootstrap-switch.js');
$doc->addStylesheet('http://vjs.zencdn.net/5.0.0/video-js.css');
$doc->addScript('components/com_tutolk/assets/js/jquery-bootstrap-timepicker.js');

$this->item->background != '' ? $bg = $this->item->background : $bg = 'noBg.jpg';
$this->item->image != '' ? $image = $this->item->image : $image = 'NoUser.png';

?>
<style>
.cardheader {
    cursor: pointer;
    background: url("<?php echo JURI::root(); ?>images/users/backgrounds/<?php echo $bg; ?>");
}
.bgUpload {
	top: 5px;
  	left: 5px;
  	position: absolute;
}
.img-upload { cursor: pointer; }
.panel-success {
  margin-top: 10px;
}
.radio label { float:left; padding: 0 35px 0 25px; }
.radio input[type=radio] { float:left; clear:none; margin: 2px 0 0 2px; }
.radio { padding-left: 0; margin-top:0; }
.cropit-image-preview {
        background-color: #f8f8f8;
        background-size: cover;
	background: url(<?php echo JURI::root(); ?>images/users/<?php echo $image; ?>);
        border: 1px solid #ccc;
        margin-top: 7px;
	margin-bottom: 15px;
        width: 200px;
        height: 200px;
        cursor: move;
	border-radius: 50% !important;
     }
#image-cropper {
  overflow: hidden;
  height: 270px;
  width: 200px;
  margin: 0 auto;
}
#map { width:320px; height:200px; }
</style>
<script>
jQuery(document).ready(function($){

//typeahead countries
		var countries = ["Afghanistan","Albania","Algeria","Andorra","Angola","Anguilla","Antigua & Barbuda","Argentina","Armenia","Aruba","Australia","Austria","Azerbaijan","Bahamas","Bahrain","Bangladesh","Barbados","Belarus","Belgium","Belize","Benin","Bermuda","Bhutan","Bolivia","Bosnia &amp; Herzegovina","Botswana","Brazil","British Virgin Islands","Brunei","Bulgaria","Burkina Faso","Burundi","Cambodia","Cameroon","Cape Verde","Cayman Islands","Chad","Chile","China","Colombia","Congo","Cook Islands","Costa Rica","Cote D Ivoire","Croatia","Cruise Ship","Cuba","Cyprus","Czech Republic","Denmark","Djibouti","Dominica","Dominican Republic","Ecuador","Egypt","El Salvador","Equatorial Guinea","Estonia","Ethiopia","Falkland Islands","Faroe Islands","Fiji","Finland","France","French Polynesia","French West Indies","Gabon","Gambia","Georgia","Germany","Ghana","Gibraltar","Greece","Greenland","Grenada","Guam","Guatemala","Guernsey","Guinea","Guinea Bissau","Guyana","Haiti","Honduras","Hong Kong","Hungary","Iceland","India","Indonesia","Iran","Iraq","Ireland","Isle of Man","Israel","Italy","Jamaica","Japan","Jersey","Jordan","Kazakhstan","Kenya","Kuwait","Kyrgyz Republic","Laos","Latvia","Lebanon","Lesotho","Liberia","Libya","Liechtenstein","Lithuania","Luxembourg","Macau","Macedonia","Madagascar","Malawi","Malaysia","Maldives","Mali","Malta","Mauritania","Mauritius","Mexico","Moldova","Monaco","Mongolia","Montenegro","Montserrat","Morocco","Mozambique","Namibia","Nepal","Netherlands","Netherlands Antilles","New Caledonia","New Zealand","Nicaragua","Niger","Nigeria","Norway","Oman","Pakistan","Palestine","Panama","Papua New Guinea","Paraguay","Peru","Philippines","Poland","Portugal","Puerto Rico","Qatar","Reunion","Romania","Russia","Rwanda","Saint Pierre &amp; Miquelon","Samoa","San Marino","Satellite","Saudi Arabia","Senegal","Serbia","Seychelles","Sierra Leone","Singapore","Slovakia","Slovenia","South Africa","South Korea","Spain","Sri Lanka","St Kitts &amp; Nevis","St Lucia","St Vincent","St. Lucia","Sudan","Suriname","Swaziland","Sweden","Switzerland","Syria","Taiwan","Tajikistan","Tanzania","Thailand","Timor L'Este","Togo","Tonga","Trinidad &amp; Tobago","Tunisia","Turkey","Turkmenistan","Turks & Caicos","Uganda","Ukraine","United Arab Emirates","United Kingdom","Uruguay","Uzbekistan","Venezuela","Vietnam","Virgin Islands (US)","Yemen","Zambia","Zimbabwe"];

		var countries = new Bloodhound({
		  datumTokenizer: Bloodhound.tokenizers.whitespace,
		  queryTokenizer: Bloodhound.tokenizers.whitespace,
		  local: countries
		});

		$('#countries .typeahead').typeahead({
		  hint: true,
		  highlight: true,
		  minLength: 1
		},
		{
		  name: 'countries',
		  source: countries
		}).on('typeahead:selected', function(e){
     			e.target.form.submit();
		});

	//time picker
	jQuery('#jform_hour_from').timepicker();
	jQuery('#jform_hour_to').timepicker();

	var tags = new Bloodhound({
	  local: <?php echo $model->getTags(); ?>,
	  datumTokenizer: function(d) {
	    return Bloodhound.tokenizers.whitespace(d);
	  },
	  queryTokenizer: Bloodhound.tokenizers.whitespace
	});

	tags.initialize();

	jQuery(".cardheader").click(function() {
	    jQuery("input[id='bg']").click();
	});

    	jQuery('#inputtags').tokenfield({
	  typeahead: [null, { source: tags.ttAdapter() }]
	});

	jQuery( ".cardheader" ).hover(function() {
	  jQuery( '.bgUpload' ).toggle();
	});

	//switches
	//jQuery.fn.bootstrapSwitch.defaults.size = 'small';
	//jQuery.fn.bootstrapSwitch.defaults.onColor = 'success';
        //jQuery("[name='jform[webcam]']").bootstrapSwitch();

        jQuery('#image-cropper').cropit({});

	jQuery('.open').click(function() {
  		jQuery('.cropit-image-input').click();
	});
	$('.save').click(function () {
            var imageData = $('#image-cropper').cropit('export', {
                type: 'image/png'
            });
            $('.hidden-image-data').val(imageData);
            var formData = new FormData();
            formData.append("imageFile", imageData);
	    jQuery('#form-user').submit();
        });

    var text_max = 140;
    var text_length = $('#jform_short_description').val().length;
    var text_remaining = text_max - text_length;
    $('#textarea_feedback').html(text_remaining + ' characters remaining');

    $('#jform_short_description').keyup(function() {
        var text_length = $('#jform_short_description').val().length;
        var text_remaining = text_max - text_length;

        $('#textarea_feedback').html(text_remaining + ' characters remaining');
    });

	$("#map").geolocate({
	lat: "#jform_latitude",
	lng: "#jform_longitude",
	markerOptions: {
		title: "This is your selected location"
	}
	});
});


function saveTags() {
    var list = jQuery('#inputtags').tokenfield('getTokensList');
    jQuery('#jform_tags').val(list);
}
</script>


<div class="user-edit front-end-edit">

<div class="col-md-6 pull-left">
	<h1>Profile</h1>
</div>
<div class="col-md-6 pull-right" style="text-align:right;">
        <a href="<?php echo JRoute::_('index.php?option=com_tutolk&view=users&Itemid=110'); ?>" class="btn btn-labeled btn-success">
		<span class="btn-label"><i class="fa fa-chevron-left"></i></span>Back
	</a>
	<a href="#" class="btn btn-labeled btn-success save">
		<span class="btn-label"><i class="fa fa-floppy-o"></i></span>Save
	</a>
	<a href="#" class="btn btn-labeled btn-success open">
		<span class="btn-label"><i class="fa fa-image"></i></span>Upload
	</a>
	<a href="#" class="btn btn-labeled btn-danger" data-toggle="modal" data-target="#deleteModal">
		<span class="btn-label-danger"><i class="fa fa-exclamation-triangle"></i></span> Delete my account
	</a>
</div>

<div class="clearfix"></div>

<form id="form-user" action="<?php echo JRoute::_('index.php?option=com_tutolk&task=profile.save'); ?>" method="post" class="form-validate form-vertical" enctype="multipart/form-data">
	<input type="hidden" name="option" value="com_tutolk" />
        <input type="hidden" name="task" value="profile.save" />
	<input type="hidden" name="jform[tags]" id="jform_tags" value="<?php echo $this->item->tags; ?>" />
        <?php echo JHtml::_('form.token'); ?>
	<?php echo $this->form->getInput('userid'); ?>
	<?php echo $this->form->getInput('id'); ?>

<div class="col-md-4">

<?php if($this->item->image == '' || $this->item->background == '' || $this->item->video == '') : ?>
<div class="panel panel-success">
	<div class="panel-heading">
        <h3 class="panel-title" id="panel-title"><i class="fa fa-graduation-cap"></i> Suggestions</h3>
      	</div>
      	<div class="panel-body">

		<?php if($this->item->image == '') : ?>
	    	<div class="desc" style="margin-bottom:10px;">
			<div class="col-md-1"><i class="fa fa-check-circle green"></i></div>
			<div class="col-md-11">Only can resize images greater than 200x200 pixels</div>
	    	</div>
		<?php endif; ?>
		<?php if($this->item->background == '') : ?>
            	<div class="desc" style="margin-bottom:10px;">
			<div class="col-md-1"><i class="fa fa-check-circle green"></i></div>
			<div class="col-md-11">Upload a cool background picture</div>
		</div>
		<?php endif; ?>
		<?php if($this->item->video == '') : ?>
	    	<div class="desc" style="margin-bottom:10px;">
			<div class="col-md-1"><i class="fa fa-check-circle green"></i></div>
			<div class="col-md-11">Upload a video explaining about you</div>
	    	</div>
		<?php endif; ?>

      	</div>
</div>
<?php endif; ?>

<div class="panel panel-success">
	<div class="panel-heading">
        <h3 class="panel-title" id="panel-title"><i class="fa fa-user-secret"></i> Privacy</h3>
      	</div>
      	<div class="panel-body">

            	<div class="clearfix" style="margin-bottom:10px;">
			<div class="col-md-5">Visible on map :</div>
			<div class="col-md-7"><?php echo $this->form->getInput('map'); ?></div>
			<div class="col-md-5">Use gravatar :</div>
			<div class="col-md-7"><?php echo $this->form->getInput('gravatar'); ?></div>
		</div>

      	</div>
</div>

<div class="panel panel-success">
	<div class="panel-heading">
        <h3 class="panel-title" id="panel-title"><i class="fa fa-cog"></i> Preferences</h3>
      	</div>
      	<div class="panel-body">


            	<div class="clearfix" style="margin-bottom:10px;">
			<div class="col-md-5">Do you have a webcam? :</div>
			<div class="col-md-7"><?php echo $this->form->getInput('webcam'); ?></div>
		</div>
	    	<div class="clearfix" style="margin-bottom:10px;">
			<div class="col-md-5">Usually connect to Tutolk from :</div>
			<div class="col-md-7"><?php echo $this->form->getInput('hour_from'); ?></div>
	    	</div>
		<div class="clearfix" style="margin-bottom:10px;">
			<div class="col-md-5">Usually connect to Tutolk to :</div>
			<div class="col-md-7"><?php echo $this->form->getInput('hour_to'); ?></div>
	    	</div>
	    	<div class="clearfix" style="margin-bottom:10px;">
			<div class="col-md-5">Timezone :</div>
			<div class="col-md-7"><?php echo $this->form->getInput('timezone'); ?></div>
	    	</div>
		<div class="clearfix" style="margin-bottom:10px;">
			<div class="col-md-5">Video presentation :</div>
			<div class="col-md-7"><input type="file" name="video" id="video" accept="video/mp4" size="10" /></div>
	    	</div>
		<div class="clearfix" style="margin-bottom:10px;">
			<div id="map"></div>
			<?php echo $this->form->getInput('latitude'); ?>
			<?php echo $this->form->getInput('longitude'); ?>
		</div>

      	</div>
</div>

<div class="panel panel-success">
	<div class="panel-heading">
        <h3 class="panel-title" id="panel-title"><i class="fa fa-video-camera"></i> Video preview</h3>
      	</div>
      	<div class="panel-body center">

		<?php if($this->item->video != '') : ?>
		<video id="my-video" class="video-js" controls preload="auto" poster="http://www.tutolk.com/images/users/videos/poster.png" data-setup="{}">
		    	<source src="http://www.tutolk.com/images/users/videos/<?php echo $this->item->video; ?>" type='video/webm'>
			<source src="http://www.tutolk.com/images/users/videos/<?php echo $this->item->video; ?>" type='video/mp4'>
		    <p class="vjs-no-js">
		      To view this video please enable JavaScript, and consider upgrading to a web browser that
		      <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
		    </p>
		  </video>
		 <script src="http://vjs.zencdn.net/5.0.0/video.js"></script>
		<?php endif; ?>
      	</div>
</div>
</div>

<div class="col-md-8">
	<div class="row">

            	<div class="card hovercard">
                <div class="cardheader hasTip" title="edit">
			<a style="display:none;" class="btn btn-circle bgUpload" title="Edit"><i class="fa fa-edit fa-2x"></i></a>
                </div>
                <div class="avatar">

			<div id="image-cropper">

			  <div class="cropit-image-preview"></div>

			  <input type="range" class="cropit-image-zoom-input custom" />

			  <input type="file" class="cropit-image-input" />
			  <input type="hidden" class="hidden-image-data" name="image-data" />
			</div>

                </div>

                <div class="info">
                    <div class="title">
                        <?php echo $this->form->getInput('name'); ?>
                    </div>
                    <div class="desc"><?php echo $this->form->getInput('age'); ?></div>
                    <div class="desc"><?php echo $this->form->getInput('sex'); ?></div>
                    <div class="desc" id="countries" style="margin-bottom:10px;"><?php echo $this->form->getInput('country'); ?></div>
		    <div class="desc" style="margin-bottom: 10px;"><input type="text" name="inputtags" autocomplete="off" placeholder="Type something and hit enter" data-limit="10" data-maxLength="10" data-beautify="false" class="form-control" id="inputtags" value="<?php echo $this->item->tags; ?>" onchange="saveTags();" /></div>
		    <div class="desc"><?php echo $this->form->getInput('short_description'); ?> <span id="textarea_feedback"></span></div>
		    <div class="desc"><?php echo $this->form->getInput('description'); ?></div>
                </div>

            </div>

	</div>
</form>
</div>

<form id="upload-bg" action="<?php echo JRoute::_('index.php?option=com_tutolk&task=profile.uploadBg'); ?>" method="post" class="form-validate form-vertical" enctype="multipart/form-data">
	<div style="display:none;"><input type="file" name="bg" id="bg" onchange="this.form.submit()" /></div>
	<input type="hidden" name="option" value="com_tutolk" />
        <input type="hidden" name="task" value="profile.uploadBg" />
        <?php echo JHtml::_('form.token'); ?>
</form>

<!-- delete Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Account delete</h4>
      </div>
      <div class="modal-body">
        <p>Are you sure you want to delete your account? If you delete your Tutolk account all your personal data will be removed from our database ... We're sorry to see you go.</p>
	<form name="pwdForm" id="pwdForm" action="" method="post">
	<label>For security reasons tell us your password:</label>
	<input type="password" name="pwd" id="pwd" value="" />
	</form>
	<script>jQuery('#pwd').keypress(function() {
			if(jQuery('#pwd').val() != '') { jQuery('#deleteBtn').removeAttr('disabled'); }
		});
	</script>
      </div>
      <div class="modal-footer">
        <a href="index.php?option=com_tutolk&task=profile.deleteAccount" class="btn btn-danger" id="deleteBtn" disabled="disabled">Delete</a>
      </div>
    </div>
  </div>
</div>

