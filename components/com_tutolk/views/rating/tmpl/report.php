<?php
/**
 * @version     1.0.0
 * @package     com_tutolk
 * @copyright   Copyright (C) 2015. Todos los derechos reservados.
 * @license     Licencia Pública General GNU versión 2 o posterior. Consulte LICENSE.txt
 * @author      aficat <kim@aficat.com> - http://www.afi.cat
 */
// no direct access
defined('_JEXEC') or die;

$app  = JFactory::getApplication();
$user = JFactory::getUser();
$model = $this->getModel();
//redirect guests users to login/register area
if($user->guest) {
	$returnurl = JRoute::_('index.php?option=com_users&view=login&return='.base64_encode(JUri::current()), false);
    	$app->redirect($returnurl, JText::_('COM_TUTOLK_LOGIN_BEFORE'));
}

$userid = JRequest::getVar('userid', 0, 'get');
?>

<section id="list">

<div class="col-md-6 pull-left">
	<h1><?php echo JText::_('COM_TUTOLK_REPORT_TEXT'); ?></h1>
</div>

<div class="col-md-6 pull-right" style="text-align:right;">

	<a href="#" onclick="report.submit();" class="btn btn-labeled btn-success">
		<span class="btn-label"><i class="fa fa-paper-plane"></i></span> Send
	</a>

</div>

<div class="clearfix"></div>

<!-- left col -->
<div class="col-md-12">

	<p><?php echo JText::_('COM_TUTOLK_REPORT_DESC'); ?></p>

	<form name="report" id="report" action="index.php?option=com_tutolk&task=sendReport" method="post">
        <input type="hidden" name="userid" value="<?php echo $userid; ?>">
        <input type="hidden" name="you" value="<?php echo JFactory::getUser()->id; ?>">
        <div class="form-group">
            <label for="descReport">Description</label>
            <textarea class="form-control" id="descReport" name="descReport" placeholder="please describe the user abuse" rows="3"></textarea>
        </div>
    </form>

</div>

</section>
