<?php
/**
 * @version     1.0.0
 * @package     com_tutolk
 * @copyright   Copyright (C) 2015. Todos los derechos reservados.
 * @license     Licencia Pública General GNU versión 2 o posterior. Consulte LICENSE.txt
 * @author      aficat <kim@aficat.com> - http://www.afi.cat
 */
// no direct access
defined('_JEXEC') or die;

$app  = JFactory::getApplication();
$user = JFactory::getUser();
$model = $this->getModel();
//redirect guests users to login/register area
if($user->guest) {
	$returnurl = JRoute::_('index.php?option=com_users&view=login&return='.base64_encode(JUri::current()), false);
    	$app->redirect($returnurl, JText::_('COM_TUTOLK_LOGIN_BEFORE'));
}

$id = $app->input->get('id', 0, 'get');
$userid = TutolkHelper::getMeetingData($id, 'userid');

if($userid == $user->id) {
	$returnurl = JRoute::_('index.php?option=com_tutolk&view=users');
    	$app->redirect($returnurl);
}
?>

<script>
jQuery(document).ready(function() {
    jQuery('input.check').rating();
	jQuery('input.check').on('change', function () {
          	var value = jQuery(this).val();
          	var userid = <?php echo $userid; ?>;
			jQuery.ajax({
			type:'GET',
			url: 'index.php?option=com_tutolk&task=user.rating&tmpl=raw',
			data: 'value=' + value + '&userid=' + userid,
			cache: false,
			success: function(response) {
			    jQuery('#thanks').css('display', 'block');
			    setTimeout(function(){ $('#thanks').fadeOut('slow'); }, 4000);
			    document.location.href = 'index.php?option=com_tutolk&view=users';
			}
		    });
        });
});
</script>

<section id="list">

<div class="col-md-6 pull-left">
	<h1><?php echo JText::_('COM_TUTOLK_RATING_TEXT'); ?></h1>
</div>

<div class="col-md-6 pull-right" style="text-align:right;">

	<a href="index.php?option=com_tutolk&view=rating&layout=report&userid=<?php echo $userid; ?>&Itemid=137" class="btn btn-labeled btn-success">
		<span class="btn-label"><i class="fa fa-calendar"></i></span> Report abuse
	</a>

</div>

<div class="clearfix"></div>

<!-- left col -->
<div class="col-md-12">

	<p><?php echo JText::_('COM_TUTOLK_RATING_DESC'); ?></p>

	<div class="rating-wrapper" style="text-align: center;">
		<input type="hidden" class="rating check" data-filled="fa fa-star fa-2x" data-empty="fa fa-star-o fa-2x" />
	</div>
	
	<div id="thanks" style="display:none;"><?php echo JText::_('COM_TUTOLK_RATING_THANKS'); ?></div>

</div>

</section>

