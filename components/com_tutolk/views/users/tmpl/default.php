<?php

/**
 * @version     1.0.0
 * @package     com_tutolk
 * @copyright   Copyright (C) 2015. Todos los derechos reservados.
 * @license     Licencia Pública General GNU versión 2 o posterior. Consulte LICENSE.txt
 * @author      aficat <kim@aficat.com> - http://www.afi.cat
 */
// No direct access
defined('_JEXEC') or die;
$app  = JFactory::getApplication();
$user = JFactory::getUser();
$model = $this->getModel();
//redirect guests users to login/register area
if($user->guest) {
	$returnurl = JRoute::_('index.php?option=com_users&view=login&return='.base64_encode(JUri::current()), false);
    	$app->redirect($returnurl, JText::_('COM_TUTOLK_LOGIN_BEFORE'));
}

$doc = JFactory::getDocument();
$doc->addStylesheet('components/com_tutolk/assets/css/ion.rangeSlider.css');
$doc->addStylesheet('components/com_tutolk/assets/css/ion.rangeSlider.skinModern.css');
$doc->addScript('components/com_tutolk/assets/js/ion.rangeSlider.min.js');

$doc->addStylesheet('templates/tutolk/css/bootstrap-tags.css');
$doc->addStylesheet('templates/tutolk/css/tokenfield-typeahead.css');
$doc->addScript('templates/tutolk/js/bootstrap-tags.js');
$doc->addScript('templates/tutolk/js/typeahead.bundle.min.js');

$listOrder	= $this->escape($this->state->get('list.ordering'));
$listDirn	= $this->escape($this->state->get('list.direction'));
$ages = explode(';', $this->state->get('filter.age', '0;99'));

$session = JFactory::getSession();
$session->clear('userslist');
$userslist = array();
$userslist[] = $user->id;
?>

<script>
jQuery( document ).ready(function() {

//typeahead countries
		var countries = ["Afghanistan","Albania","Algeria","Andorra","Angola","Anguilla","Antigua & Barbuda","Argentina","Armenia","Aruba","Australia","Austria","Azerbaijan","Bahamas","Bahrain","Bangladesh","Barbados","Belarus","Belgium","Belize","Benin","Bermuda","Bhutan","Bolivia","Bosnia &amp; Herzegovina","Botswana","Brazil","British Virgin Islands","Brunei","Bulgaria","Burkina Faso","Burundi","Cambodia","Cameroon","Cape Verde","Cayman Islands","Chad","Chile","China","Colombia","Congo","Cook Islands","Costa Rica","Cote D Ivoire","Croatia","Cruise Ship","Cuba","Cyprus","Czech Republic","Denmark","Djibouti","Dominica","Dominican Republic","Ecuador","Egypt","El Salvador","Equatorial Guinea","Estonia","Ethiopia","Falkland Islands","Faroe Islands","Fiji","Finland","France","French Polynesia","French West Indies","Gabon","Gambia","Georgia","Germany","Ghana","Gibraltar","Greece","Greenland","Grenada","Guam","Guatemala","Guernsey","Guinea","Guinea Bissau","Guyana","Haiti","Honduras","Hong Kong","Hungary","Iceland","India","Indonesia","Iran","Iraq","Ireland","Isle of Man","Israel","Italy","Jamaica","Japan","Jersey","Jordan","Kazakhstan","Kenya","Kuwait","Kyrgyz Republic","Laos","Latvia","Lebanon","Lesotho","Liberia","Libya","Liechtenstein","Lithuania","Luxembourg","Macau","Macedonia","Madagascar","Malawi","Malaysia","Maldives","Mali","Malta","Mauritania","Mauritius","Mexico","Moldova","Monaco","Mongolia","Montenegro","Montserrat","Morocco","Mozambique","Namibia","Nepal","Netherlands","Netherlands Antilles","New Caledonia","New Zealand","Nicaragua","Niger","Nigeria","Norway","Oman","Pakistan","Palestine","Panama","Papua New Guinea","Paraguay","Peru","Philippines","Poland","Portugal","Puerto Rico","Qatar","Reunion","Romania","Russia","Rwanda","Saint Pierre &amp; Miquelon","Samoa","San Marino","Satellite","Saudi Arabia","Senegal","Serbia","Seychelles","Sierra Leone","Singapore","Slovakia","Slovenia","South Africa","South Korea","Spain","Sri Lanka","St Kitts &amp; Nevis","St Lucia","St Vincent","St. Lucia","Sudan","Suriname","Swaziland","Sweden","Switzerland","Syria","Taiwan","Tajikistan","Tanzania","Thailand","Timor L'Este","Togo","Tonga","Trinidad &amp; Tobago","Tunisia","Turkey","Turkmenistan","Turks & Caicos","Uganda","Ukraine","United Arab Emirates","United Kingdom","Uruguay","Uzbekistan","Venezuela","Vietnam","Virgin Islands (US)","Yemen","Zambia","Zimbabwe"];

		var countries = new Bloodhound({
		  datumTokenizer: Bloodhound.tokenizers.whitespace,
		  queryTokenizer: Bloodhound.tokenizers.whitespace,
		  local: countries
		});

		$('#countries .typeahead').typeahead({
		  hint: true,
		  highlight: true,
		  minLength: 1
		},
		{
		  name: 'countries',
		  source: countries
		}).on('typeahead:selected', function(e){
     			e.target.form.submit();
		});

jQuery("#filter_age").ionRangeSlider({
    type: "double",
    grid: true,
    min: 0,
    max: 99,
    from: <?php echo $ages[0]; ?>,
    to: <?php echo $ages[1]; ?>,
    prefix: "Age ",
    max_postfix: "+",
    onFinish: function (data) {
        jQuery("#filter_age").val(data.from+';'+data.to);
	adminForm.submit();
    }
});
var tagslist = <?php echo $model->getTags(); ?>;
var tags = new Bloodhound({
  local: tagslist,
  datumTokenizer: function(d) {
    return Bloodhound.tokenizers.whitespace(d);
  },
  queryTokenizer: Bloodhound.tokenizers.whitespace
});

tags.initialize();

$('#filter_tags').tokenfield({
	  typeahead: [null, { source: tags.ttAdapter() }]
});

$('#filter_tags').on('tokenfield:createtoken', function (e) {
    if($.inArray(e.attrs.value, tagslist) == -1) { return false; }
});

jQuery("#pop").on("click", function() {
   jQuery('#imagemodal').modal('show');  
});

$('.listgrid').click(function() {
    $('.grid')
        .find('.col-md-3')
            .removeClass('col-md-3').addClass('col-md-12');
    $('.listgrid').addClass('active');
    $('.squaregrid').removeClass('active');
});
$('.squaregrid').click(function() {
    $('.grid')
        .find('.col-md-12')
            .removeClass('col-md-12').addClass('col-md-3');
    $('.squaregrid').addClass('active');
    $('.listgrid').removeClass('active');
});

});

function getTags() {
    var list = jQuery('#filter_tags').tokenfield('getTokensList');
    jQuery('#tags').val(list);
    adminForm.submit();
}
</script>

<section id="list">

<!-- left col -->
<div class="col-md-12">

	<!-- tags input -->
	<div class="form-group">
		<div  id="tour-1">
		
		<input type="text" name="filter_tags" data-maxLength="10" data-beautify="false" data-limit="5" class="form-control" id="filter_tags"  autocomplete="off" placeholder="Type something and hit enter" onchange="getTags()" value="<?php echo $session->get('tags'); ?>" />
		</div>
	</div>
	<!-- end tags input -->

	<!-- filters -->
	<form id="adminForm" name="adminForm" action="" method="get" class="form-inline">
	<div class="filters-advanced" id="tour-2">
        		<div id="filter-panel" class="filter-panel">
            			<div class="panel panel-default">
                			<div class="panel-body">
        					<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
						<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
						<input type="hidden" name="tags" id="tags" value="<?php echo $session->get('tags'); ?>" />

						<div class="form-group" style="margin-right:20px;">
						    <input type="text" placeholder="Search" class="form-control" name="filter_search" id="filter_search" value="<?php echo $this->state->get('filter.search'); ?>" onchange="this.form.submit()">
						</div><!-- form group [search] -->

						<div class="form-group" style="width: 30%;margin-right:20px;">
						    <input type="text" name="filter_age" id="filter_age" value="<?php echo $this->state->get('filter.age'); ?>">
						</div><!-- form group [age] -->

						<div class="form-group" style="margin-right:20px;">
						    <select class="form-control" name="filter_sex" id="filter_sex" onchange="this.form.submit()">
							<option <?php if($this->state->get('filter.sex') == "") : ?>selected<?php endif; ?> value="">Select gender</option>
							<option <?php if($this->state->get('filter.sex') == "m") : ?>selected<?php endif; ?> value="m">Male</option>
							<option <?php if($this->state->get('filter.sex') == "f") : ?>selected<?php endif; ?> value="f">Famale</option>
							<option <?php if($this->state->get('filter.sex') == "x") : ?>selected<?php endif; ?> value="x">Not specified</option>
						    </select>
						</div><!-- form group [sex] -->

						<div class="form-group" style="margin-right:20px;" id="countries">
						    <input type="text" placeholder="Country" class="form-control typeahead" name="filter_country" id="filter_country" value="<?php echo $this->state->get('filter.country'); ?>" onchange="this.form.submit()">
						</div><!-- form group [country] -->

						<div class="form-group" style="margin-right:20px;">
							<select class="form-control" name="filter_webcam" id="filter_webcam" onchange="this.form.submit()">
							<option <?php if($this->state->get('filter.webcam') == "") : ?>selected<?php endif; ?> value="">Webcam</option>
							<option <?php if($this->state->get('filter.webcam') == "1") : ?>selected<?php endif; ?> value="1">Yes</option>
							<option <?php if($this->state->get('filter.webcam') == "0") : ?>selected<?php endif; ?> value="0">No</option>
						    </select>   
						</div><!-- form group [webcam] -->

						<div class="form-group" style="margin-right:20px;">
							<select class="form-control" name="filter_online" id="filter_online" onchange="this.form.submit()">
							<option <?php if($this->state->get('filter.online') == "") : ?>selected<?php endif; ?> value="">Online</option>
							<option <?php if($this->state->get('filter.online') == "1") : ?>selected<?php endif; ?> value="1">Yes</option>
							<option <?php if($this->state->get('filter.online') == "0") : ?>selected<?php endif; ?> value="0">No</option>
						    </select>   
						</div><!-- form group [online] -->

						<div class="form-group">
						    <select id="filter_orderby" name="filter_orderby" class="form-control" onchange="this.form.submit()">
							<option <?php if($this->state->get('filter.orderby') == "id") : ?>selected<?php endif; ?> value="id">Order by</option>
						        <option <?php if($this->state->get('filter.orderby') == "name") : ?>selected<?php endif; ?> value="name">Alphabetical</option>
							<option <?php if($this->state->get('filter.orderby') == "age") : ?>selected<?php endif; ?> value="age">Age</option>
							<option <?php if($this->state->get('filter.orderby') == "sex") : ?>selected<?php endif; ?> value="sex">Sex</option>
							<option <?php if($this->state->get('filter.orderby') == "country") : ?>selected<?php endif; ?> value="country">Country</option>
						    </select>                                
						</div> <!-- form group [orderby] --> 
						<div class="form-group" style="margin-left: 40px;">
						<a href="index.php?option=com_wrapper&view=wrapper&Itemid=131">
						<i class="fa fa-globe fa-4x"></i>
						</a>
						</div>
                			</div>
            			</div>
			</div>   
	</div>
	<!-- end filters -->

	<!-- start usercards -->
	<i class="fa fa-th fa-2x squaregrid active hidden-xs hidden-sm"></i> <i class="fa fa-align-justify fa-2x listgrid hidden-xs hidden-sm"></i>
	<div class="row grid">
		<?php if(count($this->items)) : ?>
		  <?php 
		  $i = 1;
		  foreach($this->items as $item) : 
		  $userslist[] = $item->userid;
		  $model->setOrderingByTags($item->userid, $session->get('tags'));
		  $i % 2 == 0 ? $color = 'lazur' : $color = 'red';
		  ?>

	
		<div class="col-md-3 usercard">
			<a href="index.php?option=com_tutolk&view=user&id=<?php echo $item->id; ?>">
            		<div class="widget-head-color-box <?php echo $color; ?>-bg p-lg text-center">
                		<div class="m-b-md">
                			<h2 class="font-bold no-margins"><?php echo $item->name; ?></h2>
                    			<small><?php echo $item->country != '' ? $item->country : 'Unknow'; ?></small>
                		</div>
                		<img src="<?php echo TutolkHelper::getUserImage($item->userid); ?>" class="img-circle circle-border m-b-md" alt="profile">
                		<div>
                    			<span><?php echo TutolkHelper::countFriends($item->userid); ?> Friends</span> |
                    			<span><input type="hidden" class="rating check<?php echo $item->id; ?>" data-filled="fa fa-star" data-empty="fa fa-star-o" disabled="disabled" /></span> | <span><?php if(TutolkHelper::isOnline($item->userid)) : ?>Online<?php else : ?>Offline<?php endif; ?></span>
<script>jQuery('input.check<?php echo $item->id; ?>').rating('rate', <?php echo TutolkHelper::getAllRating($item->userid); ?>);</script>
                		</div>
            		</div>
			    <div class="widget-text-box">
				<h4 class="media-heading"><?php echo $item->name; ?></h4>
				<p><?php echo $item->short_description != '' ? $item->short_description : '...'; ?></p>
		
				<h3 class="font-bold">
				        <?php
					$j = 0;
					$tags = explode(',', $item->tags);
					$available = explode(',', $session->get('tags'));
					foreach($tags as $tag) : ?>
					<?php if(in_array(strtolower($tag), array_map('strtolower', $available))) : ?>
					<span class="label label-success"><?php echo $tag; ?></span>
					<?php endif; ?>
					<?php 
					$j++;
					endforeach; ?>
				</h3>
			    </div>
			</a>
        	</div>
		<!-- end usercards -->
	  <?php 
	  $i++;
	  endforeach; ?>
<?php else : ?>
<h3 class="center"><?php echo JText::_('We couldn\'t find users with these characteristics'); ?></h3>
<?php endif; ?>
</div>
<?php 
$session->set('userslist', $userslist);
if(count($this->items) < 4) : ?>
<?php 
echo $this->loadTemplate('suggested'); ?>
<?php endif; ?>

</form>
</div>
<!-- end col -->

<p class="counter">
	<?php echo $this->pagination->getPagesCounter(); ?>
</p>
<?php echo $this->pagination->getPagesLinks(); ?> 
</section>
