<?php

/**
 * @version     1.0.0
 * @package     com_tutolk
 * @copyright   Copyright (C) 2015. Todos los derechos reservados.
 * @license     Licencia Pública General GNU versión 2 o posterior. Consulte LICENSE.txt
 * @author      aficat <kim@aficat.com> - http://www.afi.cat
 */
// No direct access
defined('_JEXEC') or die;
$app  = JFactory::getApplication();
$user = JFactory::getUser();
$model = $this->getModel();
$session = JFactory::getSession();
?>

<?php if(count($model->getRandom())) : ?>
<div class="clearfix"></div>
<h3 class="center"><?php echo JText::_('Suggested users...'); ?></h3>
<?php 
	  $i = 1;
	  foreach($model->getRandom() as $row) : 
	  $model->setOrderingByTags($row->userid, $session->get('tags'));
	  $i % 2 == 0 ? $color = 'lazur' : $color = 'red';
	  ?>
          <div class="col-md-3 usercard">
		<a href="index.php?option=com_tutolk&view=user&id=<?php echo $row->id; ?>">
            	<div class="widget-head-color-box <?php echo $color; ?>-bg p-lg text-center">
                	<div class="m-b-md">
                		<h2 class="font-bold no-margins"><?php echo $row->name; ?></h2>
                    		<small><?php echo $row->country != '' ? $row->country : 'Unknow'; ?></small>
                	</div>
                	<img src="<?php echo TutolkHelper::getUserImage($row->userid); ?>" class="img-circle circle-border m-b-md" alt="profile">
                	<div>
		            <span><?php echo TutolkHelper::countFriends($row->userid); ?> Friends</span> |
		            <span><input type="hidden" class="rating check<?php echo $row->id; ?>" data-filled="fa fa-star" data-empty="fa fa-star-o" disabled="disabled"/></span> | <span><?php if(TutolkHelper::isOnline($item->userid)) : ?>Online<?php else : ?>Offline<?php endif; ?></span>
			<script>jQuery('input.check<?php echo $row->id; ?>').rating('rate', <?php echo TutolkHelper::getAllRating($row->userid); ?>);</script>
		        </div>
            	</div>
            	<div class="widget-text-box">
                	<h4 class="media-heading"><?php echo $row->name; ?></h4>
                	<p><?php echo $row->short_description; ?></p>
		
		        <h3 class="font-bold">
		                <?php
				$j = 0;
				$tags = explode(',', $row->tags);
				$available = explode(',', $session->get('tags'));
				foreach($tags as $tag) : ?>
				<?php if(in_array(strtolower($tag), array_map('strtolower', $available))) : ?>
				<span class="label label-success"><?php echo $tag; ?></span>
				<?php endif; ?>
				<?php 
				$j++;
				endforeach; ?>
		        </h3>
            	</div>
		</a>
          </div>
	  <?php 
	  $i++;
	  endforeach; ?>
<?php endif; ?>
