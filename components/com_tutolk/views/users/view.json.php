<?php

/**
 * @version     1.0.0
 * @package     com_tutolk
 * @copyright   Copyright (C) 2015. Todos los derechos reservados.
 * @license     Licencia Pública General GNU versión 2 o posterior. Consulte LICENSE.txt
 * @author      aficat <kim@aficat.com> - http://www.afi.cat
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View class for a list of Tutolk.
 */
class TutolkViewUsers extends JViewLegacy {

    protected $items;

    /**
     * Display the view
     */
    public function display($tpl = null) {
    
        $app = JFactory::getApplication();

        $data = $this->get('Items');      
 
	// Output the JSON data.
	echo json_encode($data);
    }

}
