<?php
/**
 * @version     1.0.0
 * @package     com_tutolk
 * @copyright   Copyright (C) 2015. Todos los derechos reservados.
 * @license     Licencia Pública General GNU versión 2 o posterior. Consulte LICENSE.txt
 * @author      aficat <kim@aficat.com> - http://www.afi.cat
 */
// no direct access
defined('_JEXEC') or die;

$app  = JFactory::getApplication();
$user = JFactory::getUser();
$model = $this->getModel();
//redirect guests users to login/register area
if($user->guest) {
	$returnurl = JRoute::_('index.php?option=com_users&view=login&return='.base64_encode(JUri::current()), false);
    	$app->redirect($returnurl, JText::_('COM_TUTOLK_LOGIN_BEFORE'));
}

$doc = JFactory::getDocument();
$doc->addStylesheet('http://vjs.zencdn.net/5.0.0/video-js.css');

$this->item->background != '' ? $bg = $this->item->background : $bg = 'noBg.jpg';
$friendshipStatus = TutolkHelper::friendshipStatus($this->item->userid);
//$rating = TutolkHelper::getAllRating($this->item->userid);
?>

<style>
.cover-container {
    background: url("<?php echo JURI::root(); ?>images/users/backgrounds/<?php echo $bg; ?>");
}
.btn-pref .btn {
    -webkit-border-radius:0 !important;
}
</style>

<script>
jQuery(document).ready(function() {
	jQuery(".btn-pref .btn").click(function () {
    		jQuery(".btn-pref .btn").removeClass("btn-success").addClass("btn-default");
    		jQuery(this).removeClass("btn-default").addClass("btn-success");   
	});
	jQuery('input.check').rating('rate', <?php echo $rating; ?>);
	/*
	ToDo: Pass to the rating view...
	jQuery('input.check').on('change', function () {
          	var value = jQuery(this).val();
		jQuery.ajax({
		type:'GET',
		url: 'index.php?option=com_tutolk&task=user.rating&tmpl=raw',
		data: 'value=' + value + '&userid=' + <?php echo $this->item->userid; ?>,
		cache: false,
		success: function(response) {
		    jQuery('#thanks').css('display', 'block');
		    setTimeout(function(){ $('#thanks').fadeOut('slow'); }, 7000);
		}
	    });
        });
	*/
});
</script>

<div class="col-md-6 pull-left">
	<h1><?php echo $this->item->name; ?></h1>
</div>
<div class="col-md-6 pull-right" style="text-align:right;">
        <a href="<?php echo JRoute::_('index.php?option=com_tutolk&view=users&Itemid=110'); ?>" class="btn btn-labeled btn-success">
		<span class="btn-label"><i class="fa fa-chevron-left"></i></span>Back
	</a>		
</div>

<div class="clearfix"></div>

<?php if ($this->item) : ?>

<div class="col-md-12">
   <div class="cover-container">
      <div class="social-cover"></div>
	<div class="rating-wrapper"><input type="hidden" class="rating check" data-filled="fa fa-star fa-2x" data-empty="fa fa-star-o fa-2x" disabled="disabled"/></div>
      <div class="social-avatar" >
         <img class="img-avatar" src="<?php echo TutolkHelper::getUserImage($this->item->userid); ?>">
         <h4 class="fg-white text-center"><?php echo $this->item->name; ?></h4>
         <h5 class="fg-white text-center" style="opacity:0.8;"><?php echo $this->item->country; ?></h5>
         <hr class="border-black75" style="border-width:2px;" >
         <div class="text-center">
       
			<?php if($friendshipStatus != 1) : ?>
			<a class="btn btn-inverse btn-outlined btn-retainBg btn-brightblue" href="<?php echo JRoute::_('index.php?option=com_tutolk&task=user.friendshipRequest&userid='.$this->item->id.'&'.JSession::getFormToken().'=1'); ?>" title="Request friendship">
         		<span>follow me</span>
			</a>
			<?php else : ?>
	<a class="btn btn-inverse btn-outlined btn-retainBg btn-brightblue" href="<?php echo JRoute::_('index.php?option=com_tutolk&task=user.friendshipDecline&userid='.$this->item->id.'&'.JSession::getFormToken().'=1'); ?>" title="Decline friendship"><span>unfollow</span></a>
			<?php endif; ?>	
         
         </div>
      </div>
   </div> 

    <div class="btn-pref btn-group btn-group-justified btn-group-lg" role="group" aria-label="...">
        <div class="btn-group" role="group">
            <button type="button" id="stars" class="btn btn-success" href="#tab1" data-toggle="tab"><span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                <div class="hidden-xs">Profile</div>
            </button>
        </div>
        <div class="btn-group" role="group">
            <button type="button" id="favorites" class="btn btn-default" href="#tab2" data-toggle="tab"><span class="glyphicon glyphicon-heart" aria-hidden="true"></span>
                <div class="hidden-xs">Friends</div>
            </button>
        </div>
        <div class="btn-group" role="group">
            <button type="button" id="following" class="btn btn-default <?php if($this->item->video == '') : ?>disabled<?php endif; ?>" href="#tab3" data-toggle="tab"><span class="fa fa-video-camera" aria-hidden="true"></span>
                <div class="hidden-xs">Video</div>
            </button>
        </div>
    </div>

        <div class="well">
      <div class="tab-content">
        <div class="tab-pane fade in active" id="tab1">
          
		<p><strong>Age:</strong> <?php echo $this->item->age; ?></p>
		<p><strong>Sex:</strong> <?php echo TutolkHelper::getUserSex($this->item->sex); ?></p>
		<p><strong>Country:</strong> <?php echo $this->item->country; ?></p>
		<p><strong>Usually connected from:</strong> <?php echo TutolkHelper::setHour($this->item->hour_from, $this->item->userid); ?><strong> to:</strong> <?php echo TutolkHelper::setHour($this->item->hour_to, $this->item->userid); ?></p>
		<p><strong>Description:</strong></p>
		<p><?php echo $this->item->description; ?></p>
		<div class="bottom">
			<h3 class="font-bold">
			<?php
			$tags = explode(',', $this->item->tags);
			foreach($tags as $tag) : ?>
			<span class="label label-success"><?php echo $tag; ?></span>
			<?php endforeach; ?>
			</h3>
		</div>

        </div>
        <div class="tab-pane fade in" id="tab2">
	<ul class="nav">
          <?php foreach($model->getFriends($this->item->userid) as $friend) : ?>
	<li><a href="index.php?option=com_tutolk&view=user&id=<?php echo $friend->friendid; ?>"><img width="30" height="30" src="<?php echo TutolkHelper::getUserImage($friend->usrid); ?>" alt="" class="img-circle" /> <?php echo $friend->nom; ?></a></li>
	  <?php endforeach; ?>
	</ul>
        </div>
        <div class="tab-pane fade in" id="tab3">

          	<?php if($this->item->video != '') : ?>
		<video id="my-video" class="video-js" controls preload="auto" poster="http://www.tutolk.com/images/users/videos/poster.png" data-setup="{}">
		    	<source src="http://www.tutolk.com/images/users/videos/<?php echo $this->item->video; ?>" type='video/webm'>
			<source src="http://www.tutolk.com/images/users/videos/<?php echo $this->item->video; ?>" type='video/mp4'>
		    <p class="vjs-no-js">
		      To view this video please enable JavaScript, and consider upgrading to a web browser that
		      <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
		    </p>
		  </video>     
		 <script src="http://vjs.zencdn.net/5.0.0/video.js"></script>
		<?php endif; ?>
        </div>
      </div>
</div>  
</div>

    <?php
else:
    echo JText::_('COM_TUTOLK_ITEM_NOT_LOADED');
endif;
?>
