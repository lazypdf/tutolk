<?php
/**
 * @version     1.0.0
 * @package     com_tutolk
 * @copyright   Copyright (C) 2015. Todos los derechos reservados.
 * @license     Licencia Pública General GNU versión 2 o posterior. Consulte LICENSE.txt
 * @author      aficat <kim@aficat.com> - http://www.afi.cat
 */
// no direct access
defined('_JEXEC') or die;
error_reporting(E_ALL ^ E_NOTICE);
$app  = JFactory::getApplication();
$user = JFactory::getUser();

//redirect guests users to login/register area
if($user->guest) {
	$returnurl = JRoute::_('index.php?option=com_users&view=login&return='.base64_encode(JUri::current()), false);
    	$app->redirect($returnurl, JText::_('COM_TUTOLK_LOGIN_BEFORE'));
}

$model = $this->getModel();
$id = JRequest::getVar('id', '');

//redirect guests users to login/register area
if(!TutolkHelper::isMeetingOwner($id)) {
	$returnurl = JRoute::_('index.php?option=com_tutolk&view=meetings', false);
    	$app->redirect($returnurl, JText::_('COM_TUTOLK_NO_OWNER'));
}

JHtml::_('behavior.keepalive');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');

$doc = JFactory::getDocument();
$doc->addStylesheet('templates/tutolk/css/bootstrap-datetimepicker.min.css');
$doc->addScript("templates/tutolk/js/moment.js");
$doc->addScript("templates/tutolk/js/bootstrap-datetimepicker.min.js");

?>

<script>
function validate() {
	
	var valid = false;
	var title = $('#jform_title').val();
	var data  = $('#jform_CreateDate').val();
	var attendeePW = $('#jform_attendeePW').val();
	var moderatorPW = $('#jform_moderatorPW').val();
	var duration = $('#jform_duration').val();
	var attendees = $("select[name='jform[attendees][]'] option:selected").length;
	
	if(title != '' && attendeePW != '' && moderatorPW != '' 
	&& data != '' && moment(data, 'YYYY-M-DD H:mm').isValid() && moment(data).isAfter()
	&& duration != '' && attendees != 0) {
		valid = true;
	}
	
	if(title == '') {
		$('.title-msg').html('Choose a title for this meeting');
	}
	if(attendeePW == '') {
		$('.attendeePW-msg').html('Choose a password for attendees');
	}
	if(moderatorPW == '') {
		$('.moderatorPW-msg').html('Choose a password for moderator');
	}
	if(data == '' && !moment(data, 'YYYY-M-DD H:mm').isValid() && !moment(data).isAfter()) {
		$('.date-msg').html('Date is mandatory and must be at least 15 minutes in the future');
	}
	if(attendees == 0) {
		$('.attendees-msg').html('Choose at least one friend');
	}
	
	if(valid) { meeting.submit(); }
}
</script>
<style>
.form-msg { 
	margin-bottom: 10px;
	color: #D54937;
}
</style>

<div class="user-edit front-end-edit">

<div class="col-md-6 pull-left">
	<h1>Create meeting</h1>
</div>
<div class="col-md-6 pull-right" style="text-align:right;">
        <a href="<?php echo JRoute::_('index.php?option=com_tutolk&view=meetings&Itemid=132'); ?>" class="btn btn-labeled btn-success">
		<span class="btn-label"><i class="fa fa-chevron-left"></i></span>Back
	</a>
	<a href="#" onclick="validate();" class="btn btn-labeled btn-success save">
		<span class="btn-label"><i class="fa fa-floppy-o"></i></span>Save
	</a>						
</div>

<div class="clearfix"></div>

<form id="meeting" name="meeting" action="<?php echo JRoute::_('index.php?option=com_tutolk&task=meeting.save'); ?>" method="post" class="form-validate form-vertical" enctype="multipart/form-data">
	<input type="hidden" name="option" value="com_tutolk" />
        <input type="hidden" name="task" value="meeting.save" />
	<input type="hidden" name="friends" id="friends" value="" />
        <?php echo JHtml::_('form.token'); ?>
	<?php echo $this->form->getInput('userid', '', $user->id); ?>
	<?php echo $this->form->getInput('status', '', 1); ?>
	<?php echo $this->form->getInput('id', '', $id); ?>

	<div class="col-md-8">

		<div class="desc">
			<?php echo $this->form->getLabel('title'); ?>
			<?php echo $this->form->getInput('title'); ?>
			<div class="form-msg title-msg form-msg"></div>
		</div>
		<div class="desc">
			<?php echo $this->form->getLabel('attendeePW'); ?>
			<?php echo $this->form->getInput('attendeePW'); ?>
			<div class="form-msg attendeePW-msg"></div>
		</div>
		<div class="desc">
			<?php echo $this->form->getLabel('moderatorPW'); ?>
			<?php echo $this->form->getInput('moderatorPW'); ?>
			<div class="form-msg moderatorPW-msg"></div>
		</div>
		<div class="desc">
			<?php echo $this->form->getLabel('duration'); ?>
			<?php echo $this->form->getInput('duration'); ?>
		</div>
		<div class="desc">
			<?php echo $this->form->getLabel('CreateDate'); ?>
			<div>
    <div>
        <div class="form-group">
            <div class='input-group date' id='datetimepicker'>
                <input type='text' name="jform[CreateDate]" class="form-control" id="jform_CreateDate" value="<?php echo $this->item->CreateDate; ?>" />
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar">
                    </span>
                </span>
            </div>
        </div>
        <div class="form-msg date-msg"></div>
    </div>
    <script type="text/javascript">
        $(function () {
            $('#datetimepicker').datetimepicker({
                stepping: '10',
		format: 'YYYY-M-DD H:mm',
		sideBySide: true,
		minDate: '<?php echo date("Y-m-d H:m"); ?>'
            });
        });
    </script>
</div>
		</div>
		<div class="desc">
			<?php echo $this->form->getLabel('welcome'); ?>
			<?php echo $this->form->getInput('welcome'); ?>
		</div>

	    	<div class="desc">
			<p><?php echo JText::_('COM_TUTOLK_ATTENDEES'); ?><span class="star">&nbsp;*</span></p>
			<select name="jform[attendees][]" id="jform_attendees" multiple="true">
				<?php foreach(TutolkHelper::getFriends() as $friend) : ?>
				<?php 
				$selected = '';
				if($id != '') {
					$attendees = explode(',', $this->item->attendees);
					in_array($friend->friendid, $attendees) ? $selected = 'selected' : $selected = '';
				}
				?>
				<option value="<?php echo $friend->friendid; ?>" <?php echo $selected; ?>><?php echo $friend->nom; ?></option>
				<?php endforeach; ?>
			</select>
			<div class="form-msg attendees-msg"></div>
	    	</div>
	</div>
</form>
</div>



