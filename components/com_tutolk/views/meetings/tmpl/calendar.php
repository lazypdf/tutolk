<?php

/**
 * @version     1.0.0
 * @package     com_tutolk
 * @copyright   Copyright (C) 2015. Todos los derechos reservados.
 * @license     Licencia Pública General GNU versión 2 o posterior. Consulte LICENSE.txt
 * @author      aficat <kim@aficat.com> - http://www.afi.cat
 */
// No direct access
defined('_JEXEC') or die;
$app  = JFactory::getApplication();
$user = JFactory::getUser();
$model = $this->getModel();
//redirect guests users to login/register area
if($user->guest) {
	$returnurl = JRoute::_('index.php?option=com_users&view=login&return='.base64_encode(JUri::current()), false);
    	$app->redirect($returnurl, JText::_('COM_TUTOLK_LOGIN_BEFORE'));
}

$listOrder	= $this->escape($this->state->get('list.ordering'));
$listDirn	= $this->escape($this->state->get('list.direction'));

$doc = JFactory::getDocument();
$doc->addScript('//cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js');
$doc->addScript('//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.4.0/fullcalendar.min.js');
$doc->addStylesheet('//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.4.0/fullcalendar.min.css');

?>

<script type="application/javascript">
    jQuery(document).ready(function () {
        jQuery("#calendar").fullCalendar({
		header: {
			left: 'prev,next today',
			center: 'title',
			right: 'month,agendaWeek,agendaDay'
		},
		events: [
			<?php if(count($this->items)) : 
			$i = 0;
			$color = '';
		  	foreach($this->items as $item) :
				switch($item->status) {
					case '1':
					$class = 'fce-warning';
					break;
					case '2':
					$class = 'fce-success';
					break;
					case '3':
					$class = 'fce-danger';
					break;
					default:
					$class = 'fce-warning';
					break;
				}
			?>
			{
				title: '<?php echo $item->title; ?>',
				start: '<?php echo $item->CreateDate; ?>',
				className : '<?php echo $class; ?>'
			}
			<?php 
			$i++;
			if($i < count($this->items)) { echo ','; }
			endforeach;
			endif; ?>
			]

	});
    });
</script>

<section id="list">

<div class="col-md-6 pull-left">
	<h1>Meetings</h1> 
</div>

<div class="col-md-6 pull-right" style="text-align:right;">
	<a href="<?php echo JRoute::_('index.php?option=com_tutolk&view=meetings'); ?>" class="btn btn-labeled btn-success">
		<span class="btn-label"><i class="fa fa-bars"></i></span>List view
	</a>
	<a href="<?php echo JRoute::_('index.php?option=com_tutolk&view=meeting'); ?>" class="btn btn-labeled btn-success">
		<span class="btn-label"><i class="fa fa-plus"></i></span>New
	</a>
        <a href="<?php echo JRoute::_('index.php?option=com_tutolk&view=users'); ?>" class="btn btn-labeled btn-success">
		<span class="btn-label"><i class="fa fa-chevron-left"></i></span>Back
	</a>				
</div>

<div class="clearfix"></div>

<!-- left col -->
<div class="col-md-12">

	<div id="calendar"></div>
		
</div>
<!-- end col -->

</section>
