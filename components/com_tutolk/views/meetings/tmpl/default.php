<?php

/**
 * @version     1.0.0
 * @package     com_tutolk
 * @copyright   Copyright (C) 2015. Todos los derechos reservados.
 * @license     Licencia Pública General GNU versión 2 o posterior. Consulte LICENSE.txt
 * @author      aficat <kim@aficat.com> - http://www.afi.cat
 */
// No direct access
defined('_JEXEC') or die;
$app  = JFactory::getApplication();
$user = JFactory::getUser();
$model = $this->getModel();
//redirect guests users to login/register area
if($user->guest) {
	$returnurl = JRoute::_('index.php?option=com_users&view=login&return='.base64_encode(JUri::current()), false);
    	$app->redirect($returnurl, JText::_('COM_TUTOLK_LOGIN_BEFORE'));
}

$listOrder	= $this->escape($this->state->get('list.ordering'));
$listDirn	= $this->escape($this->state->get('list.direction'));

?>

<section id="list">

<div class="col-md-6 pull-left">
	<h1>Meetings</h1> 
</div>

<div class="col-md-6 pull-right" style="text-align:right;">
	<a href="<?php echo JRoute::_('index.php?option=com_tutolk&view=meetings&layout=calendar'); ?>" class="btn btn-labeled btn-success">
		<span class="btn-label"><i class="fa fa-calendar"></i></span>Calendar view
	</a>
	<a href="<?php echo JRoute::_('index.php?option=com_tutolk&view=meeting'); ?>" class="btn btn-labeled btn-success">
		<span class="btn-label"><i class="fa fa-plus"></i></span>New
	</a>
        <a href="<?php echo JRoute::_('index.php?option=com_tutolk&view=users'); ?>" class="btn btn-labeled btn-success">
		<span class="btn-label"><i class="fa fa-chevron-left"></i></span>Back
	</a>				
</div>

<div class="clearfix"></div>

<!-- left col -->
<div class="col-md-12">

	<!-- filters -->
	<form id="adminForm" name="adminForm" action="" method="get" class="form-inline">
	<div class="filters-advanced">
        		<div id="filter-panel" class="filter-panel">
            			<div class="panel panel-default">
                			<div class="panel-body">
        					<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
						<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />

						<div class="form-group" style="margin-right:20px;">
						    <input type="text" placeholder="Search" class="form-control" name="filter_search" id="filter_search" value="<?php echo $this->state->get('filter.search'); ?>" onchange="this.form.submit()">
						</div><!-- form group [search] -->

						<div class="form-group">
						    <select id="filter_status" name="filter_status" class="form-control" onchange="this.form.submit()">
							<option <?php if($this->state->get('filter.status') == "") : ?>selected<?php endif; ?> value="">Status</option>
							<option <?php if($this->state->get('filter.status') == 1) : ?>selected<?php endif; ?> value="1">Pending</option>
						        <option <?php if($this->state->get('filter.status') == 2) : ?>selected<?php endif; ?> value="2">Running</option>
							<option <?php if($this->state->get('filter.status') == 3) : ?>selected<?php endif; ?> value="3">Finished</option>
							
						    </select>                                
						</div> <!-- form group [status] -->    

						<div class="form-group">
						    <select id="filter_type" name="filter_type" class="form-control" onchange="this.form.submit()">
							<option <?php if($this->state->get('filter.type') == "") : ?>selected<?php endif; ?> value="">Type</option>
							<option <?php if($this->state->get('filter.type') == 1) : ?>selected<?php endif; ?> value="1">Owner</option>
						        <option <?php if($this->state->get('filter.type') == 2) : ?>selected<?php endif; ?> value="2">Attendee</option>
							
						    </select>                                
						</div> <!-- form group [status] -->    
               
                			</div>
            			</div>
			</div>   
	</div>
	<!-- end filters -->

	<!-- start messages -->
	<div class="grid">
		<div class="row">
		<div class="col-md-12">
		<div class="main-box no-header clearfix">
                <div class="main-box-body clearfix">
                    <div class="table-responsive">
                        <table class="table user-list">
                            <thead>
                                <tr>
                                <th><span>Title</span></th>
                                <th><span>Date</span></th>
                                <th class="text-center"><span>Status</span></th>
                                <th><span>Attendees</span></th>
				<th>Type</th>
                                <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
		<?php if(count($this->items)) : ?>
		  <?php 
		  $i = 1;
		  foreach($this->items as $item) : ?>

                <tr>
                    <td>
                        <?= $item->title; ?>
                    </td>
                    <td><?= $item->CreateDate; ?></td>
                    <td class="text-center">
			<?php if($item->status == 1) : ?>
                        <span class="label label-warning">Pending</span>
			<?php elseif($item->status == 2) : ?>
			<span class="label label-success">Running</span>
			<?php elseif($item->status == 3) : ?>
			<span class="label label-danger">Finished</span>
			<?php endif; ?>
                    </td>
                    <td>
			<?php 
			if($item->attendees != '') :
			$attendees = explode(',', $item->attendees);
			foreach($attendees as $att) : ?>
                        <span title="<?php echo TutolkHelper::getUserData($att, 'name'); ?>"><img class="img-circle" src="<?php echo TutolkHelper::getUserImage($att); ?>" alt="" width="30" height="30" /></span>
			<?php endforeach; 
			endif;
			?>
                    </td>
		    <td>
			<?php echo $user->id == $item->userid ? '<span class="label label-default">Owner</span>' : '<span class="label label-warning">Attendee</span>'; ?>
		    </td>
                    <td style="width: 20%;">
			<?php if($user->id == $item->userid && $item->status == 1) : ?>
                        <a href="index.php?option=com_tutolk&task=meeting.edit&id=<?= $item->id; ?>" class="table-link">
                            <span class="fa-stack">
                                <i class="fa fa-square fa-stack-2x"></i>
                                <i class="fa fa-pencil fa-stack-1x fa-inverse"></i>
                            </span>
                        </a>
			<?php endif; ?>
			<?php if($user->id == $item->userid && ($item->status == 1 || $item->status == 3)) : ?>
                        <a href="index.php?option=com_tutolk&task=meeting.remove&id=<?= $item->id; ?>&<?php echo JSession::getFormToken(); ?>=1" class="table-link">
                            <span class="fa-stack">
                                <i class="fa fa-square fa-stack-2x"></i>
                                <i class="fa fa-trash-o fa-stack-1x fa-inverse"></i>
                            </span>
                        </a>
			<?php endif; ?>
			<?php if($item->status == 2) : ?>
					<?php if($user->id == $item->userid) : ?>
                        <a target="_blank" href="<?php echo TutolkHelper::getModeratorJoinUrl($item->id); ?>" class="table-link">
                    <?php else : ?>
                        <a target="_blank" href="<?php echo TutolkHelper::getAttendeeJoinUrl($item->id); ?>" class="table-link">
                    <?php endif; ?>
                            <span class="fa-stack">
                                <i class="fa fa-square fa-stack-2x"></i>
                                <i class="fa fa-sign-in fa-stack-1x fa-inverse"></i>
                            </span>
                        </a>
                        <?php if($user->id == $item->userid) : ?>
                        <a target="_blank" href="index.php?option=com_tutolk&task=meeting.end&id=<?= $item->id; ?>&<?php echo JSession::getFormToken(); ?>=1" class="table-link">
                            <span class="fa-stack">
                                <i class="fa fa-square fa-stack-2x"></i>
                                <i class="fa fa-sign-out fa-stack-1x fa-inverse"></i>
                            </span>
                        </a>
                        <?php endif; ?>
			<?php endif; ?>
                    </td>
               </tr> 
	
	  <?php 
	  $i++;
	  endforeach; ?>
	<?php endif; ?>
	</tbody>
	</table>
	</div>
	</div>
	</div>
	</div>
    </div>
</div>
</form>
</div>
<!-- end col -->

<p class="counter">
	<?php echo $this->pagination->getPagesCounter(); ?>
</p>
<?php echo $this->pagination->getPagesLinks(); ?> 
</section>

