<?php

/**
 * @version     1.0.0
 * @package     com_tutolk
 * @copyright   Copyright (C) 2015. Todos los derechos reservados.
 * @license     Licencia Pública General GNU versión 2 o posterior. Consulte LICENSE.txt
 * @author      aficat <kim@aficat.com> - http://www.afi.cat
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controller');

class TutolkController extends JControllerLegacy {

    /**
     * Method to display a view.
     *
     * @param	boolean			$cachable	If true, the view output will be cached
     * @param	array			$urlparams	An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
     *
     * @return	JController		This object to support chaining.
     * @since	1.5
     */
    public function display($cachable = false, $urlparams = false) {
        require_once JPATH_COMPONENT . '/helpers/tutolk.php';

        $view = JFactory::getApplication()->input->getCmd('view', 'users');
        JFactory::getApplication()->input->set('view', $view);

        parent::display($cachable, $urlparams);

        return $this;
    }

	public function acceptMeeting() {

		$msg = JText::_('COM_TUTOLK_MEETING_ACCEPTED');
		$this->setRedirect(JRoute::_('index.php?option=com_tutolk&view=meetings&Itemid=132'), $msg, 'success');
	}

	public function declineMeeting() {

		$id 	= JRequest::getVar('id', 0, 'get');
		$me 	= JRequest::getVar('me', 0, 'get');
		$db     = JFactory::getDbo();

		$db->setQuery('select attendees from #__tutolk_meetings where id = '.$id);
		$attendees = explode(',', $db->loadResult());
		$key = array_search($me, $attendees);
		unset($attendees[$key]);

		$new_attendees = implode(',', $attendees);
		$db->setQuery('update #__tutolk_meetings set attendees = '.$db->quote($new_attendees).' where id = '.$id);
		$db->query();

		//TODO: send message to owner when declined.

		$msg = JText::_('COM_TUTOLK_MEETING_DECLINED');
		$this->setRedirect(JRoute::_('index.php?option=com_tutolk&view=meetings&Itemid=132'), $msg, 'success');
	}

	public function getMapJson() {

		$db = JFactory::getDbo();
		$db->setQuery(	"select u.id, u.image, u.latitude, u.longitude, p.map from #__tutolk_users as u ".
				"inner join #__tutolk_user_params as p on p.userid = u.userid ".
				"where u.latitude != '' and u.longitude != '' and u.status = 1 and p.map = 1"
		);
		$rows = $db->loadAssocList();
		foreach($rows as $row) {
			$params = json_decode($row['params']);
			if($params->map == 0) {
				unset($row);
			}
		}
		echo json_encode($rows);
	}

	public function goRate() {

		$db = JFactory::getDbo();
		$id = JRequest::getVar('id', '', 'get');

		$db->setQuery('UPDATE #__tutolk_meetings SET status = 3 WHERE id = '.$id);
		$db->query();

		$this->setRedirect(JRoute::_('index.php?option=com_tutolk&view=rating&id='.$id.'&Itemid=136'));
	}

    public function sendReport() {

        $userid = JRequest::getVar('userid');
        $you    = JRequest::getVar('you');
        $descReport = JRequest::getVar('descReport');

        $subject = 'New abuse report';
        $body    = 'The user '.JFactory::getUser($you)->username-' with userid '.$you.' report an abuse from the user '.JFactory::getUser($userid)->username.' with id '.$userid.', this is the report text:<p>'.$descReport.'</p>';
        $email   = 'info@tutolk.com';

        TutolkHelper::sendEmail($subject, $body, $email);
	$msg = JText::_('COM_TUTOLK_REPORT_MSG');
        $this->setRedirect('index.php?option=com_tutolk&view=rating&layout=report&userid='.$userid.'&Itemid=137', $msg);

    }

	public function createMeetings() {

		$db = JFactory::getDbo();
		jimport('bbb.bbb-api');
		$bbb = new BigBlueButton();

		$time 	 = date('Y-m-d H:i:s');
		$prefix  = 'tutolk';

		//check meetings...
		$db->setQuery("SELECT * FROM #__tutolk_meetings WHERE CreateDate >= DATE_SUB(CURDATE(), INTERVAL 15 MINUTE) AND status = 1");
		$rows = $db->loadObjectList();

		foreach($rows as $row) {

			$logouturl = 'http://www.tutolk.com/index.php?option=com_tutolk&task=goRate&id='.$row->id;

			//create meeting...
			$creationParams = array(
				'meetingName' => $row->title,	// REQUIRED
				'meetingId' => $prefix.$row->id,	// REQUIRED
				'attendeePw' => $row->attendeePW, // Match this value in getJoinMeetingURL() to join as attendee.
				'moderatorPw' => $row->moderatorPW, // Match this value in getJoinMeetingURL() to join as moderator.
				'welcomeMsg' => $row->welcome,	// ''= use default. Change to customize.
				'logoutUrl' => $logouturl,	// Default in bigbluebutton.properties. Optional.
				'maxParticipants' => '-1',	// Optional. -1 = unlimitted. Not supported in BBB. [number]
				'record' => 'false',		// New. 'true' will tell BBB to record the meeting.
				'duration' => $row->duration // Default = 0 which means no set duration in minutes. [number]
			);

			$result = $bbb->createMeetingWithXmlResponseArray($creationParams);
			TutolkHelper::addLogEntry('result '.$result['returncode']);
			if($result['returncode'] == 'SUCCESS') {

				//update status to 'running'...
				$db->setQuery('UPDATE #__tutolk_meetings SET status = 2 WHERE id = '.$row->id);
				$db->query();

				$username = JFactory::getUser($row->userid)->username;

				//send notification to owner...
				$joinParams = array(
					'meetingId' => $prefix.$row->id, 	// REQUIRED - We have to know which meeting to join.
					'username' => $username, // REQUIRED - User name that will show in meeting.
					'password' => $row->moderatorPW, // REQUIRED - Must match either moderator pass for meeting.
					//'createTime' => $time, // OPTIONAL - string
					'userId' => $row->userid,	// OPTIONAL - string
					'webVoiceConf' => ''		// OPTIONAL - string
				);

				$from 	 = 0; //0 = Tutolk
				$link    = $bbb->getJoinMeetingURL($joinParams);
				$subject = JText::_('COM_TUTOLK_CREATE_MEETING_SUBJECT');
				$body 	 = JText::sprintf('COM_TUTOLK_CREATE_MEETING_BODY', $link);
				$email   = JFactory::getUser($row->userid)->email;

				//notice owner with message...
				TutolkHelper::sendMessage($row->userid, $from, $subject, $body, time());

				//notice owner with email...
				TutolkHelper::sendEmail($subject, $body, $email);

				//explode attendees to send notifications...
				$attendees = explode(',', $row->attendees);
				foreach($attendees as $attendee) {

					$att_username = JFactory::getUser($attendee)->username;

					$joinParams = array(
						'meetingId' => $prefix.$row->id, 	// REQUIRED - We have to know which meeting to join.
						'username' => $att_username,	// REQUIRED - The user name show in the BBB meeting.
						'password' => $row->attendeePW, // REQUIRED - Must match either attendee pass for meeting.
						//'createTime' => $time, // OPTIONAL - string
						'userId' => $attendee,		// OPTIONAL - string
						'webVoiceConf' => ''		// OPTIONAL - string
					);

					$link    = $bbb->getJoinMeetingURL($joinParams);
					$body 	 = JText::sprintf('COM_TUTOLK_CREATE_MEETING_BODY', $link);

					//notice attendees with message...
					TutolkHelper::sendMessage($attendee, $from, $subject, $body, time());

					//notice attendees with email...
					TutolkHelper::sendEmail($subject, $body, JFactory::getUser($attendee)->email);

				}//end attendees loop

			}//end if create meeting
			else {
				//meeting not created, write in log...
				TutolkHelper::addLogEntry('Error creating meeting '.$row->id);
			}

		}//end meetings loop
	}

}

