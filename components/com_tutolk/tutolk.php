<?php
/**
 * @version     1.0.0
 * @package     com_tutolk
 * @copyright   Copyright (C) 2015. Todos los derechos reservados.
 * @license     Licencia Pública General GNU versión 2 o posterior. Consulte LICENSE.txt
 * @author      aficat <kim@aficat.com> - http://www.afi.cat
 */

defined('_JEXEC') or die;

// Include dependancies
jimport('joomla.application.component.controller');
define('DS', DIRECTORY_SEPARATOR );
require_once JPATH_COMPONENT.'/helpers/tutolk.php';
// Execute the task.
$controller	= JControllerLegacy::getInstance('Tutolk');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
