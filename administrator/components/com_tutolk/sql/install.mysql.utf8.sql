CREATE TABLE IF NOT EXISTS `#__tutolk_users` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`userid` INT(11)  NOT NULL ,
`sex` CHAR(7)  NOT NULL ,
`short_description` VARCHAR(255)  NOT NULL ,
`description` TEXT NOT NULL ,
`age` INT(5)  NOT NULL ,
`country` VARCHAR(150)  NOT NULL ,
`image` VARCHAR(255)  NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
`created_by` INT(11)  NOT NULL ,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT COLLATE=utf8_general_ci;

