<?php

/**
* @version		$Id: com_jpframework  Kim $
* @package		com_jpframework v 1.0.0
* @copyright	Copyright (C) 2014 Afi. All rights reserved.
* @license		GNU/GPL, see LICENSE.txt
*/

// restricted access
defined('_JEXEC') or die('Restricted access');

$blockid = JRequest::getVar('blockid');
blocksHelper::loadCss(JURI::root().'administrator/components/com_jpframework/blocks/carousel/assets/css/carousel.css');
blocksHelper::getBlockParameter($blockid,'carousel_play') == 0 ? $play = "false" : $play = blocksHelper::getBlockParameter($blockid,'carousel_height');
?>

<style>
.carousel-<?php echo $blockid; ?> .slides .slide-1 {
  background-image: url(<?php echo JURI::root().blocksHelper::getBlockParameter($blockid,'carousel_img1'); ?>);
}
.carousel-<?php echo $blockid; ?> {
    height: <?php echo blocksHelper::getBlockParameter($blockid,'carousel_height'); ?>px;
}
.carousel-<?php echo $blockid; ?> .carousel-inner .item {
    height: <?php echo blocksHelper::getBlockParameter($blockid,'carousel_height'); ?>px;
}
.carousel-<?php echo $blockid; ?> .slides .slide-1 {
  height: <?php echo blocksHelper::getBlockParameter($blockid,'carousel_height'); ?>px;
}
</style>

<section id="<?php echo blocksHelper::getBlockParameter($blockid, 'uniqid', 'block-'.$blockid); ?>">

<div class="carousel fade-carousel slide carousel-<?php echo $blockid; ?>" data-ride="carousel" data-interval="<?php echo $play; ?>" id="bs-carousel">

  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <div class="item slides active">
      <div class="slide-1"></div>
      <div class="hero">
        <hgroup>
            <h1><?php echo blocksHelper::getBlockParameter($blockid,'carousel_title1'); ?></h1>
            <h3><?php echo blocksHelper::getBlockParameter($blockid,'carousel_text1'); ?></h3>
	    <?php if(JFactory::getUser()->guest) : ?>
	    <a href="index.php?option=com_users&view=login" class="btn btn-success">Start for free</a>
	    <?php else : ?>
	    <a href="index.php?option=com_tutolk&view=users&Itemid=110" class="btn btn-success">Enter</a>
	    <?php endif; ?>
        </hgroup>
      </div>
    </div>

  </div>

</div>

</section>
