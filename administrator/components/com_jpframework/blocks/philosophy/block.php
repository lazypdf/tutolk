<?php

/**
 * @version     1.0.0
 * @package     com_jpframework
 * @copyright   Copyright (C) 2015. Todos los derechos reservados.
 * @license     Licencia Pública General GNU versión 2 o posterior. Consulte LICENSE.txt
 * @author      aficat <kim@aficat.com> - http://www.afi.cat
 */
// No direct access
defined('_JEXEC') or die;
require_once(JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_jpframework' . DS . 'helpers' . DS . 'blocks.php');
$blockid = JRequest::getVar('blockid');

?>

<section id="<?php echo blocksHelper::getBlockParameter($blockid, 'uniqid', 'block-'.$blockid); ?>" style="background-color:<?php echo blocksHelper::getBlockParameter($blockid,'block_color'); ?>">
	<div class="container" id="features">

<header>
<h1><?php echo blocksHelper::getBlockParameter($blockid, 'blocktitle'); ?></h1>
<p class="lead"><?php echo blocksHelper::getBlockParameter($blockid, 'subtitle'); ?></p>
</header>

<div class="col-md-12 text-center">
	<button class="btn btn-inverse wow tada" id="readmore">Read more...</button>
</div>

<div class="row" id="philosophybox" style="display:none;">

<div class="col-md-12 text-center">
	<?php echo blocksHelper::getBlockParameter($blockid, 'description'); ?>
</div>

</div><!-- /row -->
</div><!-- /container -->
</section>
