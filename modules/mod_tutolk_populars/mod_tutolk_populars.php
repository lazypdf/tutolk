<?php

/**
* @version		$Id: mod_tutolk_users  Kim $
* @package		mod_tutolk_users v 1.0.0
* @copyright		Copyright (C) 2014 Afi. All rights reserved.
* @license		GNU/GPL, see LICENSE.txt
*/

// restriccion de acceso
defined('_JEXEC') or die('Acceso Restringido');
define('DS', DIRECTORY_SEPARATOR );
require_once (dirname(__FILE__).DS.'helper.php');
require_once (JPATH_ROOT.DS.'components'.DS.'com_tutolk'.DS.'helpers'.DS.'tutolk.php');

require( JModuleHelper::getLayoutPath( 'mod_tutolk_populars', 'default') );

?>
