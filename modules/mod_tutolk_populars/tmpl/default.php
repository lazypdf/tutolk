<?php

/**
* @version		$Id: mod_tutolk_users  Kim $
* @package		mod_tutolk_users v 1.0.0
* @copyright		Copyright (C) 2014 Afi. All rights reserved.
* @license		GNU/GPL, see LICENSE.txt
*/

// restricted access
defined('_JEXEC') or die('Restricted access');
$class_sfx	= htmlspecialchars($params->get('moduleclass_sfx'));

?>

<div class="panel panel-success">
	<div class="panel-heading">
        <h3 class="panel-title" id="panel-title"><i class="fa fa-star"></i> <?php echo JText::_('COM_TUTOLK_MOST_POPULAR'); ?></h3>
      	</div>
      	<div class="panel-body center"> 

<?php foreach( modTutolkPopularsHelper::getMostPopular() as $p ) : ?>

			<!-- start user -->
			<div class="col-md-12" style="margin-bottom: 15px;">
			<a href="index.php?option=com_tutolk&view=user&id=<?php echo TutolkHelper::getUserData($p['userid'], 'id'); ?>">
            		<div class="col-md-12"><img width="150" height="150" class="img-circle" src="<?php echo TutolkHelper::getUserImage($p['userid']); ?>" alt="" style="min-width:25px;" title="<?php echo TutolkHelper::getUserData($p['userid'], 'name'); ?>" /></div> 
                	<div class="col-md-12">	        
                    		<span><input type="hidden" class="rating check<?php echo $p['userid']; ?>" data-filled="fa fa-star" data-empty="fa fa-star-o" disabled="disabled" /></span>
<script>jQuery('input.check<?php echo $p["userid"]; ?>').rating('rate', <?php echo TutolkHelper::getAllRating($p['userid']); ?>);</script>
                	</div>
			</a>
        		</div>
			<!-- end user -->

<?php endforeach; ?>

</div>
</div>

