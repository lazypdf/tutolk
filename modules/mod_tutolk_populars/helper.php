<?php

/**
* @version		$Id: mod_tutolk_users  Kim $
* @package		mod_tutolk_users v 1.0.0
* @copyright		Copyright (C) 2014 Afi. All rights reserved.
* @license		GNU/GPL, see LICENSE.txt
*/

// restricted access
defined('_JEXEC') or die('Acceso Restringido');

class modTutolkPopularsHelper
{
	public static function getMostPopular() {
				
		$db   = JFactory::getDbo();
		$user = JFactory::getUser();

		$db->setQuery('select distinct(userid) from #__tutolk_users as u inner join #__tutolk_rating as r on r.user_to = u.userid where u.userid != '.$user->id);
		$rows = $db->loadObjectList();

		$i = 0;

		foreach($rows as $row) {

			$result[$i]['rating'] = TutolkHelper::getAllRating($row->userid);
			$result[$i]['userid'] = $row->userid;
			$i++;
		}

		arsort($result);
		array_slice($result, 0, 3);

		return $result;
	}
}

?>
