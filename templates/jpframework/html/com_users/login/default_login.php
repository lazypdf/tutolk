<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::script('templates/jpframework/html/com_users/login/slogin.js');
$user = JFactory::getUser();
    $input = new JInput;

    $callbackUrl = '';

    $dispatcher	= JDispatcher::getInstance();

    JPluginHelper::importPlugin('slogin_auth');

    $plugins = array();

    $dispatcher->trigger('onCreateSloginLink', array(&$plugins, $callbackUrl));
    $profileLink = $avatar = '';
    if(JPluginHelper::isEnabled('slogin_integration', 'profile') && $user->id > 0){
        require_once JPATH_BASE.'/plugins/slogin_integration/profile/helper.php';
        $profile = plgProfileHelper::getProfile($user->id);
        $avatar = isset($profile->avatar) ? $profile->avatar : '';
        $profileLink = isset($profile->social_profile_link) ? $profile->social_profile_link : '';
    }
    else if(JPluginHelper::isEnabled('slogin_integration', 'slogin_avatar') && $user->id > 0){
        require_once JPATH_BASE.'/plugins/slogin_integration/slogin_avatar/helper.php';
        $path = Slogin_avatarHelper::getavatar($user->id);
        if(!empty($path['photo_src'])){
            $avatar = $path['photo_src'];
            if(JString::strpos($avatar, '/') !== 0)
                $avatar = '/'.$avatar;
        }
		$profileLink = isset($path['profile']) ? $path['profile'] : '';
    }
?>

<style>
@media (min-width: 768px) {
    .omb_row-sm-offset-3 div:first-child[class*="col-"] {
        margin-left: 25%;
    }
}

.omb_login .omb_authTitle {
    text-align: center;
	line-height: 300%;
}
	
.omb_login .omb_socialButtons a {
	color: white; // In yourUse @body-bg 
	opacity:0.9;
}
.omb_login .omb_socialButtons a:hover {
    color: white;
	opacity:1;    	
}
.omb_login .omb_socialButtons .omb_btn-facebook {background: #3b5998;}
.omb_login .omb_socialButtons .omb_btn-twitter {background: #00aced;}
.omb_login .omb_socialButtons .omb_btn-google {background: #c32f10;}


.omb_login .omb_loginOr {
	position: relative;
	font-size: 1.5em;
	color: #aaa;
	margin-top: 1em;
	margin-bottom: 1em;
	padding-top: 0.5em;
	padding-bottom: 0.5em;
}
.omb_login .omb_loginOr .omb_hrOr {
	background-color: #cdcdcd;
	height: 1px;
	margin-top: 0px !important;
	margin-bottom: 0px !important;
}
.omb_login .omb_loginOr .omb_spanOr {
	display: block;
	position: absolute;
	left: 50%;
	top: -0.6em;
	margin-left: -1.5em;
	background-color: white;
	width: 3em;
	text-align: center;
}			

.omb_login .omb_loginForm .input-group.i {
	width: 2em;
}
.omb_login .omb_loginForm  .help-block {
    color: red;
}

	
@media (min-width: 768px) {
    .omb_login .omb_forgotPwd {
        text-align: right;
		margin-top:10px;
 	}		
}
</style>

<div class="container login">
   
    <div class="omb_login" id="slogin-buttons">
    	<h3 class="omb_authTitle">Login or <a href="<?php echo JRoute::_('index.php?option=com_users&view=registration&Itemid=105'); ?>">Sign up</a></h3>
		<div class="row omb_row-sm-offset-3 omb_socialButtons">
		<?php
        foreach($plugins as $link):
            $linkParams = '';
            if(isset($link['params'])){
                foreach($link['params'] as $k => $v){
                    $linkParams .= ' ' . $k . '="' . $v . '"';
                }
            }
			$title = (!empty($link['plugin_title'])) ? ' title="'.$link['plugin_title'].'"' : '';
            ?>
		<?php
		if($link['class'] == "facebookslogin") { $title = 'Facebook'; $icon = 'facebook'; $class = 'facebook'; }
		if($link['class'] == "googleslogin") { $title = 'Google+'; $icon = 'google-plus'; $class = 'google'; }
		if($link['class'] == "twitterslogin") { $title = 'Twitter'; $icon = 'twitter'; $class = 'twitter'; }
		?>
		<div class="col-xs-4 col-sm-2">
		        <a rel="nofollow" href="<?php echo JRoute::_($link['link']);?>" class="btn btn-lg btn-block omb_btn-<?php echo $class; ?> <?php echo $link['class'];?>">
			        <i class="fa fa-<?php echo $icon; ?> visible-xs"></i>
			        <span class="hidden-xs"><?php echo $title; ?></span>
		        </a>
	        </div>
        <?php endforeach; ?> 
    		
		</div>

		<div class="row omb_row-sm-offset-3 omb_loginOr">
			<div class="col-xs-12 col-sm-6">
				<hr class="omb_hrOr">
				<span class="omb_spanOr">or</span>
			</div>
		</div>

		<div class="row omb_row-sm-offset-3">
			<div class="col-xs-12 col-sm-6">	
			    <form class="omb_loginForm form-validation" action="<?php echo JRoute::_('index.php?option=com_users&task=user.login'); ?>" autocomplete="off" method="POST">
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-user"></i></span>
						<input type="text" class="form-control" name="username" placeholder="Username">
					</div>
					<span class="help-block"></span>
										
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-lock"></i></span>
						<input  type="password" class="form-control" name="password" placeholder="Password">
					</div>
                    			<span class="help-block"></span>

					<button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
					<?php echo JHtml::_('form.token'); ?>
					<input type="hidden" name="return" value="<?php echo base64_encode($this->params->get('login_redirect_url', $this->form->getValue('return'))); ?>" />
				</form>
			</div>
    	</div>
		<div class="row omb_row-sm-offset-3">
			<div class="col-xs-12 col-sm-3">
				<label class="checkbox">
					<input type="checkbox" id="remember" name="rememeber" value="yes"><?php echo JText::_('COM_USERS_LOGIN_REMEMBER_ME') ?>
				</label>
			</div>
			<div class="col-xs-12 col-sm-3">
				<p class="omb_forgotPwd">
					<a href="<?php echo JRoute::_('index.php?option=com_users&view=reset'); ?>"><?php echo JText::_('COM_USERS_LOGIN_RESET'); ?></a></a>
				</p>
			</div>
		</div>	    	
	</div>
</div>
