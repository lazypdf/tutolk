<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_contact
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$cparams = JComponentHelper::getParams('com_media');

jimport('joomla.html.html.bootstrap');
?>

<style>
.omb_btn-facebook { background: #3b5998; }
.omb_btn-twitter { background: #00aced; }
.omb_btn-google { background: #c32f10; }
</style>

<section id="faq">

        <div class="container">

            <header>
                <h1><?php echo $this->escape($this->params->get('page_heading')); ?></h1>
                <div class="lead"><?php echo $this->contact->misc; ?></div>
            </header>

            <div class="row">

                <div class="col-md-6">             

                    <div class="alert">
                        
                        <p class="lead"><?php echo JText::_('JP_FRAMEWORK_CONTACT_DETAILS'); ?></p>
                        <div class="row">
                          
                          <div class="col-md-12">
		           	<a rel="nofollow" href="#" target="_blank" class="btn btn-lg btn-block omb_btn-facebook">
			        <i class="fa fa-facebook"></i>
			        <span class="hidden-xs">Facebook</span>
		        	</a>
	        	    </div>
				<div class="col-md-12">
		           	<a rel="nofollow" href="https://twitter.com/tutolk" target="_blank" class="btn btn-lg btn-block omb_btn-twitter">
			        <i class="fa fa-twitter"></i>
			        <span class="hidden-xs">Twitter</span>
		        	</a>
	        	    </div>
				<div class="col-md-12">
		           	<a rel="nofollow" href="#" target="_blank" class="btn btn-lg btn-block omb_btn-google">
			        <i class="fa fa-google-plus"></i>
			        <span class="hidden-xs">Google+</span>
		        	</a>
	        	    </div>
	
	                        </div><!-- /row -->
			

                    </div><!-- /alert -->

                </div><!-- /col-md-6 -->

                <div class="col-md-6">

            <div class="row contact-intro">
			<?php $this->contact->image == '' ? $c_img = 'http://preview.simonswiss.com/booom/images/icons/support.svg' : $c_img = $this->contact->image; ?>
                        <div class="col-md-3 text-right"><img src="<?php echo $c_img; ?>" alt=""></div>
                        <div class="col-md-9"><p class="lead"><?php echo JText::_('JP_FRAMEWORK_CONTACT_FORM_TITLE'); ?></p></div>                        
                    </div>

                    <!--////////// CONTACT FORM //////////-->
                    <form id="contact-form" action="<?php echo JRoute::_('index.php'); ?>" method="post" class="form-validate form-horizontal">
                        <input type="text" name="jform[contact_name]" id="jform_contact_name" required="required" aria-required="true" aria-invalid="true" class="required invalid form-control input-hg" placeholder="<?php echo JText::_('JP_FRAMEWORK_CONTACT_FORM_NAME'); ?>">
                        <input type="text" id="jform_contact_email" name="jform[contact_email]" required="required" aria-required="true" class="required validate-email form-control input-hg" placeholder="<?php echo JText::_('JP_FRAMEWORK_CONTACT_FORM_EMAIL'); ?>">
						<input type="text" id="jform_contact_subject" name="jform[contact_subject]" required="required" aria-required="true" class="required form-control input-hg" placeholder="<?php echo JText::_('JP_FRAMEWORK_CONTACT_FORM_SUBJECT'); ?>">
                        <textarea class="required form-control input-hg" rows="4" id="jform_contact_message" required="required" aria-required="true" name="jform[contact_message]" placeholder="<?php echo JText::_('JP_FRAMEWORK_CONTACT_FORM_MSG'); ?>"></textarea>
                        <button type="submit" class="btn btn-inverse btn-hg btn-block" name="submit"><?php echo JText::_('COM_CONTACT_CONTACT_SEND'); ?></button>
						<input type="hidden" name="option" value="com_contact" />
						<input type="hidden" name="task" value="contact.submit" />
						<input type="hidden" name="return" value="<?php echo $this->return_page; ?>" />
						<input type="hidden" name="id" value="<?php echo $this->contact->slug; ?>" />
						<?php echo JHtml::_('form.token'); ?>
                    </form>

                     <div id="contact-error"></div>
                    <!--////////// end CONTACT FORM ///////////-->

                </div><!-- /col-md-6-->
                
            </div>
        </div>

    </section>
