<?php
/**
 * @version    	jp_framework.php $ kim 2011-02-02 15:57
 * @package		JPFramework
 * @copyright   Copyright © 2010 - All rights reserved.
 * @license		GNU/GPL
 * @author		kim
 * @author mail kim@aficat.com
 * @website		http://www.afi.cat
 *
*/

defined('_JEXEC') or die;
$app = JFactory::getApplication();
?>
<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Cookie" type="text/css" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" type="text/css" />
<title><?php echo htmlspecialchars($app->getCfg('sitename')); ?></title>

<style>
body{
	background-color: #5ead68;
	color: #fff;
}
h1 { font-family: 'Cookie'; font-size: 100px; }
#coming-soon { width: 30%; margin: 0 auto; text-align: center; }
.small { margin-top: 10px; }
.login-icon { position: absolute; top: 10px; left: 10px; }
</style>
</head>
<body>

<jdoc:include type="message" />

<div class="login-icon" onclick="document.getElementById('subscribe').style.display = 'block';"><span class="glyphicon glyphicon-user"></span></div> 

<div id="coming-soon">

    <h1><?php echo htmlspecialchars($app->getCfg('sitename')); ?></h1>
	
	<?php if ($app->getCfg('display_offline_message', 1) == 1 && str_replace(' ', '', $app->getCfg('offline_message')) != ''): ?>
	<p><?php echo $app->getCfg('offline_message'); ?></p>
	<?php elseif ($app->getCfg('display_offline_message', 1) == 2 && str_replace(' ', '', JText::_('JOFFLINE_MESSAGE')) != ''): ?>
	<p><?php echo JText::_('JOFFLINE_MESSAGE'); ?></p>
	<?php  endif; ?>
	
	<div id="subscribe" style="display:none;">
    	<form action="<?php echo JRoute::_('index.php', true); ?>" method="post" id="form-login" class="form-inline">
		<div class="form-group">
			<input class="form-control" name="username" id="username" type="text" placeholder="<?php echo JText::_('JGLOBAL_USERNAME') ?>" />
		</div>
		<div class="form-group">
			<input class="form-control" type="password" name="password" placeholder="<?php echo JText::_('JGLOBAL_PASSWORD') ?>" id="passwd" />
		</div>
			<input type="submit" name="Submit" class="btn btn-default" value="<?php echo JText::_('JLOGIN') ?>" />
		    <input type="hidden" name="option" value="com_users" />
		    <input type="hidden" name="task" value="user.login" />
		    <input type="hidden" name="return" value="<?php echo base64_encode(JURI::base()) ?>" />
		    <?php echo JHtml::_('form.token'); ?>
    	</form>
	</div>

	<p><h3>Let’s stay in touch</h3></p>

	<?php
	jimport( 'joomla.application.module.helper' );
	$module = JModuleHelper::getModule( 'mailchimpsignup');
	echo JModuleHelper::renderModule( $module );
	?>

	<p class="small">We'll never share your email address and you can opt out at any time, we promise.</p>
	
</body>
</html>
