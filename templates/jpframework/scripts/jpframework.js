// Initialize WOW.js Scrolling Animations
new WOW().init();

jQuery(document).ready(function($){
	// browser window scroll (in pixels) after which the "back to top" link is shown
	var offset = 300,
		//browser window scroll (in pixels) after which the "back to top" link opacity is reduced
		offset_opacity = 1200,
		//duration of the top scrolling animation (in ms)
		scroll_top_duration = 700,
		//grab the "back to top" link
		$back_to_top = $('.cd-top');

	//hide or show the "back to top" link
	$(window).scroll(function(){
		( $(this).scrollTop() > offset ) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
		if( $(this).scrollTop() > offset_opacity ) { 
			$back_to_top.addClass('cd-fade-out');
		}
	});

	//smooth scroll to top
	$back_to_top.on('click', function(event){
		event.preventDefault();
		$('body,html').animate({
			scrollTop: 0 ,
		 	}, scroll_top_duration
		);
	});

});

//Performs a smooth page scroll to an anchor on the same page.
jQuery(function() {
  jQuery('a[href*=#]:not([href=#])').click(function() {
	if( jQuery(this).attr("href")=="#bs-carousel") return;//This is the exception
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = jQuery(this.hash);
      target = target.length ? target : jQuery('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        jQuery('html,body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
});

//display our philosophy box
jQuery(document).ready(function($){
	jQuery( "#readmore" ).click(function() {
		if($('#philosophybox').css('display') == 'none')
		{
			jQuery( "#philosophybox" ).fadeIn(1200).css('display', 'block');
			jQuery('#readmore').html('Read less...');
		} else {
			jQuery( "#philosophybox" ).fadeIn(1200).css('display', 'none');
			jQuery('#readmore').html('Read more...');
		}
	});
});
