/**
 * @version     1.0.0
 * @package     tutolk
 * @copyright   Copyright (C) 2015. Todos los derechos reservados.
 * @license     Licencia Pública General GNU versión 2 o posterior. Consulte LICENSE.txt
 * @author      aficat <kim@aficat.com> - http://www.afi.cat
 */

(function($)
{		
	//switches
	$(document).ready(function()
	{

	    $('.btn-toggle').click(function() {
	    $(this).find('.btn').toggleClass('active');  
	    
	    if ($(this).find('.btn-success').size()>0) {
	    	$(this).find('.btn').toggleClass('btn-success');
	    }
	    
	    $(this).find('.btn').toggleClass('btn-default');
	       
	});

		//toggle friends list
	    	jQuery('#friends').click(function() {
	    		jQuery("#friendslist").toggle();
		});

		//check friends status
		setInterval(function() {
			var listItems = jQuery("#friendslist li").find("i");
        		listItems.each(function(idx, i) {
				var item   	= $(i);
    				var userid 	= item.attr('data-userid');
				var name   	= item.attr('data-username');
				var offline   	= 0;
				var online    	= 0;
				//console.log(userid + ' ' + name);
    				jQuery.ajax({
					type:'GET',
        				dataType: 'json',
        				url: 'index.php?option=com_tutolk&task=users.isOnline&tmpl=raw',
        				data: 'userid=' + userid,
        				cache: false,
        				success: function(response) {
						if(response.status == 'online') {
							if(item.hasClass('offline')) { offline = 1; }
							item.removeClass('offline');
							item.addClass('online');
							if(item.hasClass('online')) { online = 1; }
							if(offline == 1 && online == 1) {
								var audio = new Audio('http://www.tutolk.com/sounds/beep.mp3');
								audio.play();
								$('#msg').html(name + ' is online');
								setTimeout(function(){ $(".messages-box").toggleClass("toggled"); }, 2000);
							}
						} else {
							item.removeClass('online');
							item.addClass('offline');
						}
					}
    				});
    				jQuery.ajax({
					type:'GET',
        				dataType: 'json',
        				url: 'index.php?option=com_tutolk&task=message.getMessages&tmpl=raw',
        				data: '',
        				cache: false,
        				success: function(response) {
						if(response.count > 0) {
							var audio = new Audio('http://www.tutolk.com/sounds/beep.mp3');
							audio.play();
							shake();
							$('#msg').html('<a href="index.php?option=com_tutolk&view=messages&Itemid=130">You have '+response.count+' new unread messages</a>');
							var counter = $('.badge-counter').html();
							var sum = parseInt(counter) + parseInt(response.count);
							$('.badge-counter').html(sum);
							setTimeout(function(){ $(".messages-box").toggleClass("toggled"); }, 2000);
						}
					}
    				});
			});
		}, 5000); //15 seconds
	})
})(jQuery);

function shake(){
	for (var i = 0; i < 2; i++)
    		$('.notification-icon').animate({
        		left: (i % 2 === 0 ? "-" : "+") + "=50"
    		}, 200);
}

$(document).ready(function(){
    $('.hasTooltip').tooltip(); 
});

