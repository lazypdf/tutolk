<?php

/**
 * @version		jp_framework.php $ kim 2014-12-05 15:57
 * @package		JPFramework
 * @copyright   Copyright © 2014 - All rights reserved.
 * @license		GNU/GPL
 * @author		kim
 * @author mail kim@aficat.com
 * @website		http://www.afi.cat
 *
*/


class tpl
{ 
	public static function getUserImage($userid=null) {

		$db   = JFactory::getDbo();
		$user = JFactory::getUser();
		
		if($userid == null) { $userid = $user->id; }
		
		if($userid == 0) { return 'images/users/tutolk.png'; }
		
		//check gravatar
		$db->setQuery("select gravatar from #__tutolk_user_params where userid = ".$userid);
		if($db->loadResult() == 1) {

			$userEmail = JFactory::getUser($userid)->id;
			$size = "150";
			$defaultAvatar = "http://www.tutolk.com/images/users/NoUser.png";
			$userEmail = strtolower($userEmail);
			$userEmailHash = md5($userEmail);
			$defaultAvatar = urlencode($defaultAvatar); 
			return "https://gravatar.com/avatar/$userEmailHash?d=$defaultAvatar&s=$size";
			
		} else {
			//check own image
			$db->setQuery("select image from #__tutolk_users where userid = ".$userid);
			$image = $db->loadResult();
			if($image != '') {
				return 'images/users/'.$image;
			} else {
				return 'images/users/NoUser.png';
			}
		}
	}

	public static function getFriends() {

		$user = JFactory::getUser();
		$db   = JFactory::getDbo();

		$db->setQuery(  "select u.name as nom, f.friendid, u.image from #__tutolk_friends as f ".
				"inner join #__tutolk_users as u on f.friendid = u.userid ".
				"where f.status = 1 and f.userid = ".$user->id);
		return $db->loadObjectList();
	}

	public static function countFriends() {

		$user = JFactory::getUser();
		$db   = JFactory::getDbo();

		$db->setQuery('select count(friendid) from #__tutolk_friends where userid = '.$user->id.' and status = 1');
		return $db->loadResult();
	}

	public static function countMessages() {

		$user = JFactory::getUser();
		$db   = JFactory::getDbo();

		$db->setQuery('select count(id) from #__tutolk_messages where userid = '.$user->id.' and status = 0');
		return $db->loadResult();
	}

	public static function getTutolkId($userid) {

		$db   = JFactory::getDbo();
		$db->setQuery('select id from #__tutolk_users where userid = '.$userid);
		return $db->loadResult();
	}

	// get online status
	public static function isOnline($userid) {
		$db	= JFactory::getDbo();
		$query	= $db->getQuery(true);
		$query->select('session_id');
		$query->from('#__session');
		$query->where('userid = '.$userid);
		$query->where('client_id = 0');
		$db->setQuery($query);
		if($db->loadResult() != null) {
			return true;
		} 
		return false;
	}
}
