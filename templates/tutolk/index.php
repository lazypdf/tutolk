<?php 
error_reporting(E_ALL ^ E_NOTICE);
//TODO: pass this functions to tutolk helper.
require('tpl.class.php'); 
// Get params
$tour     = $this->params->get('tour', 1);
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap Core CSS -->
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
    <!-- Fontawesome CSS -->
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- Google font -->
    <link href="//fonts.googleapis.com/css?family=Cookie" rel="stylesheet">
    <!-- jQuery -->
    <script src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/js/jquery.js"></script>
    <jdoc:include type="head" />

    <!-- Tour -->
    <link href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/bootstrap-tour.min.css" rel="stylesheet">
    <!-- Rating -->
    <link href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/bootstrap-rating.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/template.css" rel="stylesheet">
    <link href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/sidebar.css" rel="stylesheet">
    <!-- Bootstrap Core JavaScript -->
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js" type="text/javascript"></script>
    
    <!-- Tour -->
    <script src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/js/bootstrap-tour.js"></script>
    <!-- Rating -->
    <script src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/js/bootstrap-rating.js"></script>
    <!-- Template -->
    <script src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/js/template.js"></script>
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

    <div id="wrapper" class="toggled">

        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                <li class="userpic">
			<div class="profile-userpic">
				<?php 
				$avatar = tpl::getUserImage();
				$avatar != '' ? $image = $avatar : $image = 'NoUser.png'; ?>
				<a href="index.php?option=com_tutolk&view=profile&Itemid=112"><img src="<?php echo $image; ?>" alt=""></a>
			</div>
                </li>
                <li>
                    <a href="index.php?option=com_tutolk&view=profile&Itemid=112">Profile</a>
                </li>
		<li>
                    <a href="index.php?option=com_tutolk&view=meetings&Itemid=132">Meetings</a>
                </li>
		<li>
                    <a href="index.php?option=com_tutolk&view=messages&Itemid=130">Messages <span class="label label-danger"><?php echo tpl::countMessages(); ?></span></a>
                </li>
                <li>
                    <a href="#" id="friends">Friends <span class="label label-danger"><?php echo tpl::countFriends(); ?></span></a>
                </li>
		
		<div id="friendslist" style="display:none;">
		<?php foreach(tpl::getFriends() as $friend) : ?>

			<li>
			<?php $friend->image != '' ? $image = $friend->image : $image = 'NoUser.png'; ?>
			<a href="index.php?option=com_tutolk&view=user&id=<?php echo tpl::getTutolkId($friend->friendid); ?>"><img width="30" height="30" class="img-circle" src="images/users/<?php echo $image; ?>" alt="" />  <?php echo $friend->nom; ?> <i data-userid="<?php echo $friend->friendid; ?>" data-username="<?php echo $friend->nom; ?>" class="fa fa-circle <?php if(tpl::isOnline($friend->friendid)) : ?>online<?php else: ?>offline<?php endif; ?>"></i></a> <a href="index.php?option=com_tutolk&view=message&to=<?php echo $friend->friendid; ?>&Itemid=130"><i class="fa fa-envelope"></i></a>
			</li>
			
		<?php endforeach; ?>
		</div>
            </ul>
        </div>
        <!-- /#sidebar-wrapper -->

	<div class="container-fluid headerbar">
		<!-- Header -->
		<header class="header" role="banner">
			<div class="header-inner clearfix">
				<a href="#menu-toggle" id="menu-toggle"><i class="fa fa-bars fa-2x"></i></a>
				<a class="brand" href="<?php echo $this->baseurl; ?>/">
					Tutolk
				</a>
				<div class="header-search pull-right">
					<?php if($tour) : ?>
					<a href="#" id="start-tour">
						<i class="fa fa-graduation-cap fa-2x"></i>
					</a>
					<?php endif; ?>
					<a href="index.php?option=com_tutolk&view=users&Itemid=110">
						<i class="fa fa-home fa-2x"></i>
					</a>
					<a href="index.php?option=com_tutolk&view=messages&filter=meeting&Itemid=130">
						<i class="fa fa-video-camera fa-2x"></i>
					</a>
					<a href="index.php?option=com_tutolk&view=messages&Itemid=130" id="messages">
					<div class="notification-icon">
					    <i class="fa fa-envelope fa-2x"></i>
					    	<?php if(tpl::countMessages() > 0) : ?>
					    	<span class="badge badge-counter"><?php echo tpl::countMessages(); ?></span>
						<?php endif; ?>
					</div>	
					</a>
					<a href="index.php?option=com_users&task=user.logout&<?php echo JSession::getFormToken(); ?>=1">
						<i class="fa fa-sign-out fa-2x"></i>
					</a>
				</div>
			</div>
		</header>
	</div>

        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-<?php if($this->countModules('rightside')) : ?>10<?php else: ?>12<?php endif; ?>">
                        <!-- Begin Content -->
			<jdoc:include type="message" />
			<jdoc:include type="component" />
			<!-- End Content -->
                    </div>
		<?php if($this->countModules('rightside')) : ?>
		    <div class="col-lg-2">
			<jdoc:include type="modules" name="rightside" />
		     </div>
		<?php endif; ?>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->
	<script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });

    // Instance the tour
	var tour = new Tour({
	  steps: [
	  {
	    element: "#tour-0",
	    title: "",
	    content: ""
	  },
	  {
	    element: "#tour-1",
	    title: "Filter by tags",
	    content: "Search people by tags"
	  },
	  {
	    element: "#tour-2",
	    title: "Add some extra filters",
	    content: "xxx"
	  }
	]});

	$("#start-tour").click(function(e) {
	// Initialize the tour
	tour.init();
	tour.start();
	});
    </script>

<div class="container messages-box">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet portlet-default">
                <div class="portlet-heading">
                    <div class="portlet-title">
			<div class="portlet-chevron">
                        <a onclick="$('.messages-box').toggleClass('toggled');" href="#" class="chevron"><i class="fa fa-times"></i></a>
                    	</div>
                    </div>		 
                </div>
                <div id="chat" class="panel-collapse collapse in">
                    <div>
                    <div class="portlet-body chat-widget" style="overflow-y: auto; width: auto; height: 300px;">
			
			<div id="msg"></div>		

                    </div>
                    </div>
			
                </div>
            </div>
        </div>
        <!-- /.col-md-4 -->
    </div>
</div>

</body>

</html>
